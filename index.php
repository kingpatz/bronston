<?php

define('BASEPATH', '');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));

function autoloader($class) {
    $class = strtolower($class);
    $file = ROOT . DS . 'lib' . DS . $class . '.php';
    require_once $file;
}

spl_autoload_register('autoloader');
require_once ROOT . DS . 'lib' . DS . 'config.php';

$page = new Init();
unset($page);
