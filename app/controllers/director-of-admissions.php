<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Director_of_admissions extends Controller {

    function index() {
        $this->load->view('director-of-admissions', ['title' => 'Director of Admissions']);
    }
}