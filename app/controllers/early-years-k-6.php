<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Early_years_k_6 extends Controller {

	function index() {
		$this->load->view('early-years-k-6', ['title' => 'Early Years (K-6)']);
	}
}
