<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Scholarship_application_steps extends Controller {

	function index() {
		$this->load->view('scholarship-application-steps', ['title' => 'Scholarship Application Steps']);
	}
}
