<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Camp extends Controller {

	function index() {
		$this->load->view('camp', ['title' => 'Camp']);
	}
}
