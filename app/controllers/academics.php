<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Academics extends Controller {

	function index() {
		$this->load->view('academics', ['title' => 'Academics']);
	}
}
