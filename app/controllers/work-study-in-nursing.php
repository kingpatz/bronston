<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Work_study_in_nursing extends Controller {

	function index() {
		$this->load->view('work-study-in-nursing', ['title' => 'Work & Study in Nursing']);
	}
}
