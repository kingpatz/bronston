<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Accreditation extends Controller {

    function index() {
        $this->load->view('accreditation', ['title' => 'Accreditation']);
    }
}