<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Academic_challenge_program extends Controller {

	function index() {
		$this->load->view('academic-challenge-program', ['title' => 'Academic Challenge Program']);
	}
}
