<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Study_permit_considerations extends Controller {

	function index() {
		$this->load->view('study-permit-considerations', ['title' => 'Study Permit Considerations']);
	}
}
