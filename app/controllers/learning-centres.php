<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Learning_centres extends Controller {

	function index() {
		$this->load->view('learning-centres', ['title' => 'Learning Centres']);
	}
}
