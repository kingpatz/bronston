<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Scholarships extends Controller {

	function index() {
		$this->load->view('scholarships', ['title' => 'Scholarships']);
	}
}
