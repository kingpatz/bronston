<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Faqs extends Controller {

	function index() {
		$this->load->view('faqs', ['title' => 'FAQ\'s']);
	}
}
