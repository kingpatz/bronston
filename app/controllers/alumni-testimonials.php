<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Alumni_testimonials extends Controller {

	function index() {
		$this->load->view('alumni-testimonials', ['title' => 'Alumni Testimonials']);
	}
}
