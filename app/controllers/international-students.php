<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class International_students extends Controller {

	function index() {
		$this->load->view('international-students', ['title' => 'International Students']);
	}
}
