<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Faculty extends Controller {

    function index() {
        $this->load->view('faculty', ['title' => 'Faculty']);
    }
}