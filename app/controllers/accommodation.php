<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Accommodation extends Controller {

	function index() {
		$this->load->view('accommodation', ['title' => 'Accommodation']);
	}
}
