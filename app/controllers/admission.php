<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Admission extends Controller {

	function index() {
		$this->load->view('admission', ['title' => 'Admission']);
	}
}
