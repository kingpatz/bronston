<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Language_studies extends Controller {

	function index() {
		$this->load->view('language-studies', ['title' => 'Languages']);
	}
}
