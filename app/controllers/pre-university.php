<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Pre_university extends Controller {

	function index() {
		$this->load->view('pre-university', ['title' => 'Pre-University']);
	}
}
