<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Blended_academics extends Controller {

	function index() {
		$this->load->view('blended-academics', ['title' => 'Blended Academics']);
	}
}
