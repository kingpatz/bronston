<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Partnerships extends Controller {

	function index() {
		$this->load->view('partnerships', ['title' => 'Partnerships']);
	}
}
