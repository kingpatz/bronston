<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Bronzi_program extends Controller {

	function index() {
		$this->load->view('bronzi-program', ['title' => 'Bronzi Program']);
	}
}
