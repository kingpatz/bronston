<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Contact_us extends Controller {

    function index() {
        $this->load->view('contact-us', ['title' => 'Contact Us']);
    }
}
