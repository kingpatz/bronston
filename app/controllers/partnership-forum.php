<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Partnership_forum extends Controller {

	function index() {
		$this->load->view('partnership-forum', ['title' => 'Partnership Forum']);
	}
}
