<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Bronston_scholars_club extends Controller {

	function index() {
		$this->load->view('bronston-scholars-club', ['title' => 'Bronston Scholars Club']);
	}
}
