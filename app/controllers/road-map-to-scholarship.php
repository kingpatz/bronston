<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Road_map_to_scholarship extends Controller {

	function index() {
		$this->load->view('road-map-to-scholarship', ['title' => 'Road Map to Scholarship']);
	}
}
