<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Application_requirements extends Controller {

	function index() {
		$this->load->view('application-requirements', ['title' => 'Application Requirements']);
	}
}
