<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class On_line_school extends Controller {

	function index() {
		$this->load->view('on-line-school', ['title' => 'On-line School']);
	}
}
