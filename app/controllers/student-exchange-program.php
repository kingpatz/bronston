<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Student_exchange_program extends Controller {

	function index() {
		$this->load->view('student-exchange-program', ['title' => 'Student Exchange Program']);
	}
}
