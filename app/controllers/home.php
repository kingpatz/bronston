<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Home extends Controller {

	function index() {
		$this->load->view('home', ['title' => 'Home']);
	}
}
