<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Alumni extends Controller {

	function index() {
		$this->load->view('alumni', ['title' => 'Alumni']);
	}
}
