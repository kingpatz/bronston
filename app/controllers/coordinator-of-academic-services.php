<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Coordinator_of_academic_services extends Controller {

	function index() {
		$this->load->view('coordinator-of-academic-services', ['title' => 'Coordinator of Academic Services']);
	}
}
