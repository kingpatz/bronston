<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Our_teaching_learning_methodology extends Controller {

	function index() {
		$this->load->view('our-teaching-learning-methodology', ['title' => 'Our Teaching/Learning Methodology']);
	}
}
