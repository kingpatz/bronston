<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Study_permit_online_application_guide extends Controller {

	function index() {
        $file = 'docs/study-permit-online-application-guide.docx';

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
	}
}
