<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Why_bronston extends Controller {

	function index() {
		$this->load->view('why-bronston', ['title' => 'Why Bronston']);
	}
}
