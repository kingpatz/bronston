<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Our_history extends Controller {

    function index() {
        $this->load->view('our-history', ['title' => 'Our History']);
    }
}