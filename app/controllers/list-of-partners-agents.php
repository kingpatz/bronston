<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class List_of_partners_agents extends Controller {

	function index() {
		$this->load->view('list-of-partners-agents', ['title' => 'List of Partners & Agents']);
	}
}
