<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Director_of_recruiting extends Controller {

	function index() {
		$this->load->view('director-of-recruiting', ['title' => 'Director of Recruiting']);
	}
}
