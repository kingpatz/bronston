<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <base href="<?= BASEPATH ?>" />
    <!-- [if lt IE 9]
    <script defer="true" src="<?= BASEPATH ?>/assets/js/html5shiv.min.js"></script>
    <script defer="true" src="<?= BASEPATH ?>/assets/js/html5shiv-printshiv.min.js"></script>
    [endif] -->
    <link rel="shortcut icon" href="<?= BASEPATH ?>/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<?= BASEPATH ?>/assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= BASEPATH ?>/assets/css/bootstrap-dropdownhover.min.css">
    <link rel="stylesheet" href="<?= BASEPATH ?>/assets/css/main.css" />
    <script defer="true" src="<?= BASEPATH ?>/assets/js/jquery.min.js"></script>
    <script defer="true" src="<?= BASEPATH ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    <script defer="true" src="<?= BASEPATH ?>/assets/js/bootstrap-dropdownhover.min.js"></script>
    <script defer="true" src="<?= BASEPATH ?>/assets/js/main.js"></script>
    <title><?= @$data['title'] ?> - Bronston</title>
</head>
<body>
<div class="wrapper">
    <header class="container">
        <div id="site-identity-wrapper">
            <div id="site-identity">
                <figure>
                    <img src="<?= BASEPATH ?>/assets/images/logo2.png" alt="Bronston logo">
                </figure>
                <h1>Bronston</h1>
                <h2 class="pull-right">Canadian Academy</h2>
            </div>
        </div>
        <div id="banner-carousel" class="carousel slide hidden-sm hidden-xs" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#banner-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#banner-carousel" data-slide-to="1"></li>
                <li data-target="#banner-carousel" data-slide-to="2"></li>
                <li data-target="#banner-carousel" data-slide-to="3"></li>
            </ol>

            <div class="carousel-inner">
                <div class="item active">
                    <img src="<?= BASEPATH ?>/assets/images/banner/IMG_0187.JPG" />
                </div>

                <div class="item">
                    <img src="<?= BASEPATH ?>/assets/images/banner/IMG_0161.JPG" />
                </div>

                <div class="item">
                    <img src="<?= BASEPATH ?>/assets/images/banner/IMG_0305.JPG" />
                </div>

                <div class="item">
                    <img src="<?= BASEPATH ?>/assets/images/banner/IMG_0371.JPG" />
                </div>
            </div>
        </div>
        <nav class="navbar navbar-inverse hidden-sm hidden-xs">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="primary-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="<?= BASEPATH ?>/home" class="dropdown-toggle" data-hover="dropdown">
                                Home
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= BASEPATH ?>/accreditation">Accreditation</a></li>
                                <li><a href="<?= BASEPATH ?>/our-history">Our History</a></li>
                                <li><a href="<?= BASEPATH ?>/faculty">Faculty</a></li>
                                <li><a href="<?= BASEPATH ?>/director-of-admissions">Director of Admissions</a></li>
                                <li><a href="<?= BASEPATH ?>/director-of-recruiting">Director of Recruiting</a></li>
                                <li><a href="<?= BASEPATH ?>/coordinator-of-academic-services">Coordinator of Academic Services</a></li>
                                <li><a href="<?= BASEPATH ?>/why-bronston">Why Bronston</a></li>
                                <li><a href="<?= BASEPATH ?>/faqs">FAQ's</a></li>
                                <li><a href="<?= BASEPATH ?>/alumni">Alumni</a></li>
                                <li><a href="<?= BASEPATH ?>/alumni-testimonials">Alumni Testimonials</a></li>
                                <li><a href="<?= BASEPATH ?>/schools">Schools</a></li>
                                <li><a href="<?= BASEPATH ?>/early-years-k-6">Early Years (K-6)</a></li>
                                <li><a href="<?= BASEPATH ?>/junior-high">Junior High</a></li>
                                <li><a href="<?= BASEPATH ?>/senior-high">Senior High</a></li>
                                <li><a href="<?= BASEPATH ?>/pre-university">Pre-University</a></li>
                                <li><a href="<?= BASEPATH ?>/language-studies">Language Studies</a></li>
                            </ul>
                        </li>
                        <li><a href="<?= BASEPATH ?>/bronston-scholars-club">Bronston Scholars Club</a></li>
                        <li><a href="<?= BASEPATH ?>/avenues-to-enrolment">Avenues to Enrolment</a></li>
                        <li class="dropdown">
                            <a href="<?= BASEPATH ?>/academics" class="dropdown-toggle" data-hover="dropdown">
                                Academics
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= BASEPATH ?>/ossd">OSSD</a></li>
                                <li><a href="<?= BASEPATH ?>/academic-challenge-program">Academic Challenge Program</a></li>
                                <li><a href="<?= BASEPATH ?>/bronzi-program">Bronzi Program</a></li>
                                <li><a href="<?= BASEPATH ?>/our-teaching-learning-methodology">Our Teaching/Learning Methodology</a></li>
                                <li><a href="<?= BASEPATH ?>/student-exchange-program">Student Exchange Program</a></li>
                                <li><a href="<?= BASEPATH ?>/blended-academics">Blended Academics</a></li>
                                <li><a href="<?= BASEPATH ?>/on-line-school">On-line School</a></li>
                                <li><a href="<?= BASEPATH ?>/post-grad-admission-through-bronston">Post-Grad Admission Through Bronston</a></li>
                                <li><a href="<?= BASEPATH ?>/work-study-in-nursing">Work & Study in Nursing</a></li>
                                <li><a href="<?= BASEPATH ?>/home-schooling">Home Schooling</a></li>
                                <li><a href="<?= BASEPATH ?>/esl">ESL</a></li>
                                <li><a href="<?= BASEPATH ?>/fsl">FSL</a></li>
                                <li><a href="<?= BASEPATH ?>/camp">Camp</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="<?= BASEPATH ?>/admission" class="dropdown-toggle" data-hover="dropdown">
                                Admission
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= BASEPATH ?>/admission-process">Admission Process</a></li>
                                <li><a href="<?= BASEPATH ?>/application-requirements">Application Requirements</a></li>
                                <li><a href="<?= BASEPATH ?>/accommodation">Accommodation</a></li>
                                <li><a href="<?= BASEPATH ?>/study-plans-fees">Study Plans & Fees</a></li>
                                <li><a href="<?= BASEPATH ?>/fee-payment-schedule">Fee Payment/Schedule</a></li>
                                <li><a href="<?= BASEPATH ?>/international-students">International Students</a></li>
                                <li><a href="<?= BASEPATH ?>/study-permit-considerations">Study Permit Considerations</a></li>
                                <li><a href="<?= BASEPATH ?>/scholarships">Scholarships</a></li>
                                <li><a href="<?= BASEPATH ?>/bwes-award">BWES Award</a></li>
                                <li><a href="<?= BASEPATH ?>/bweps-award">BWEPS Award</a></li>
                                <li><a href="<?= BASEPATH ?>/bwjs-award">BWJS Award</a></li>
                                <li><a href="<?= BASEPATH ?>/bneas-award">BNEAS Award</a></li>
                                <li><a href="<?= BASEPATH ?>/bwbs-award">BWBS Award</a></li>
                                <li><a href="<?= BASEPATH ?>/road-map-to-scholarship">Road Map to Scholarship</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="<?= BASEPATH ?>/learning-centres" class="dropdown-toggle" data-hover="dropdown">
                                Learning Centres
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= BASEPATH ?>/partnerships">Partnerships</a></li>
                                <li><a href="<?= BASEPATH ?>/list-of-partners-agents">List of Partners & Agents</a></li>
                                <li><a href="<?= BASEPATH ?>/partnership-forum">Partnership Forum</a></li>
                                <li><a href="<?= BASEPATH ?>/contact-us">Contact Us</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- sm xs nav -->

        <nav class="navbar navbar-inverse hidden-md hidden-lg">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu-sm-xs">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="primary-menu-sm-xs">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="<?= BASEPATH ?>/home" class="dropdown-toggle" data-toggle="dropdown">
                                Home
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="hidden-md hidden-lg"><a href="<?= BASEPATH ?>/">Home</a></li>
                                <li><a href="<?= BASEPATH ?>/accreditation">Accreditation</a></li>
                                <li><a href="<?= BASEPATH ?>/our-history">Our History</a></li>
                                <li><a href="<?= BASEPATH ?>/faculty">Faculty</a></li>
                                <li><a href="<?= BASEPATH ?>/director-of-admissions">Director of Admissions</a></li>
                                <li><a href="<?= BASEPATH ?>/director-of-recruiting">Director of Recruiting</a></li>
                                <li><a href="<?= BASEPATH ?>/coordinator-of-academic-services">Coordinator of Academic Services</a></li>
                                <li><a href="<?= BASEPATH ?>/why-bronston">Why Bronston</a></li>
                                <li><a href="<?= BASEPATH ?>/faqs">FAQ's</a></li>
                                <li><a href="<?= BASEPATH ?>/alumni">Alumni</a></li>
                                <li><a href="<?= BASEPATH ?>/alumni-testimonials">Alumni Testimonials</a></li>
                                <li><a href="<?= BASEPATH ?>/schools">Schools</a></li>
                                <li><a href="<?= BASEPATH ?>/early-years-k-6">Early Years (K-6)</a></li>
                                <li><a href="<?= BASEPATH ?>/junior-high">Junior High</a></li>
                                <li><a href="<?= BASEPATH ?>/senior-high">Senior High</a></li>
                                <li><a href="<?= BASEPATH ?>/pre-university">Pre-University</a></li>
                                <li><a href="<?= BASEPATH ?>/language-studies">Language Studies</a></li>
                            </ul>
                        </li>
                        <li><a href="<?= BASEPATH ?>/bronston-scholars-club">Bronston Scholars Club</a></li>
                        <li><a href="<?= BASEPATH ?>/avenues-to-enrolment">Avenues to Enrolment</a></li>
                        <li class="dropdown">
                            <a href="<?= BASEPATH ?>/academics" class="dropdown-toggle" data-toggle="dropdown">
                                Academics
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="hidden-md hidden-lg"><a href="<?= BASEPATH ?>/academics">Academics</a></li>
                                <li><a href="<?= BASEPATH ?>/ossd">OSSD</a></li>
                                <li><a href="<?= BASEPATH ?>/academic-challenge-program">Academic Challenge Program</a></li>
                                <li><a href="<?= BASEPATH ?>/bronzi-program">Bronzi Program</a></li>
                                <li><a href="<?= BASEPATH ?>/our-teaching-learning-methodology">Our Teaching/Learning Methodology</a></li>
                                <li><a href="<?= BASEPATH ?>/student-exchange-program">Student Exchange Program</a></li>
                                <li><a href="<?= BASEPATH ?>/blended-academics">Blended Academics</a></li>
                                <li><a href="<?= BASEPATH ?>/on-line-school">On-line School</a></li>
                                <li><a href="<?= BASEPATH ?>/post-grad-admission-through-bronston">Post-Grad Admission Through Bronston</a></li>
                                <li><a href="<?= BASEPATH ?>/work-study-in-nursing">Work & Study in Nursing</a></li>
                                <li><a href="<?= BASEPATH ?>/home-schooling">Home Schooling</a></li>
                                <li><a href="<?= BASEPATH ?>/esl">ESL</a></li>
                                <li><a href="<?= BASEPATH ?>/fsl">FSL</a></li>
                                <li><a href="<?= BASEPATH ?>/camp">Camp</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="<?= BASEPATH ?>/admission" class="dropdown-toggle" data-toggle="dropdown">
                                Admission
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="hidden-md hidden-lg"><a href="<?= BASEPATH ?>/admission">Admission</a></li>
                                <li><a href="<?= BASEPATH ?>/admission-process">Admission Process</a></li>
                                <li><a href="<?= BASEPATH ?>/application-requirements">Application Requirements</a></li>
                                <li><a href="<?= BASEPATH ?>/accommodation">Accommodation</a></li>
                                <li><a href="<?= BASEPATH ?>/study-plans-fees">Study Plans & Fees</a></li>
                                <li><a href="<?= BASEPATH ?>/fee-payment-schedule">Fee Payment/Schedule</a></li>
                                <li><a href="<?= BASEPATH ?>/international-students">International Students</a></li>
                                <li><a href="<?= BASEPATH ?>/study-permit-considerations">Study Permit Considerations</a></li>
                                <li><a href="<?= BASEPATH ?>/scholarships">Scholarships</a></li>
                                <li><a href="<?= BASEPATH ?>/bwes-award">BWES Award</a></li>
                                <li><a href="<?= BASEPATH ?>/bweps-award">BWEPS Award</a></li>
                                <li><a href="<?= BASEPATH ?>/bwjs-award">BWJS Award</a></li>
                                <li><a href="<?= BASEPATH ?>/bneas-award">BNEAS Award</a></li>
                                <li><a href="<?= BASEPATH ?>/bwbs-award">BWBS Award</a></li>
                                <li><a href="<?= BASEPATH ?>/road-map-to-scholarship">Road Map to Scholarship</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="<?= BASEPATH ?>/learning-centres" class="dropdown-toggle" data-toggle="dropdown">
                                Learning Centres
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="hidden-md hidden-lg"><a href="<?= BASEPATH ?>/learning-centres">Learning Centres</a></li>
                                <li><a href="<?= BASEPATH ?>/partnerships">Partnerships</a></li>
                                <li><a href="<?= BASEPATH ?>/list-of-partners-agents">List of Partners & Agents</a></li>
                                <li><a href="<?= BASEPATH ?>/partnership-forum">Partnership Forum</a></li>
                                <li><a href="<?= BASEPATH ?>/contact-us">Contact Us</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    </header>

    <main class="container">
