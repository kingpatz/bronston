<h2>Our Teaching Learning Methodology</h2>

<div class="row">
    <div class="col-md-8">
        <p>
        At <abbr title="Bronston Canadian Academy">B.C.A.</abbr> our
        Teaching/Learning Methodology is based on the principles of Self Directed
        Learning, known to <abbr title="Bronston Canadian Academy">B.C.A.</abbr>
        as <q>U Direct Learning</q>. At <abbr title="Bronston Canadian Academy">B.C.A.</abbr>
        we believe that preparing students to gain admissions into university
        programs is half the battle. Preparing students to manage the rigorous
        coursework and learning environment at the university is vital to the
        future and academic success of students. To this end, we employ the use of
        teaching/learning strategies that prepare the students for the task ahead.

        <hr />

        <h3><q>U Direct Learning</q></h3>

        <p>
            Our self directed teaching/learning program, or  <q>U Direct Learning</q>
            is based on the concept that students learn better in an environment where
            they are encouraged to play a major role in their learning process. The
            concept of self directed learning is not new and has been explored by
            several educational organizations and educators, to achieve the best
            teaching, learning methodology possible. With the help of  guidance
            counsellors and teachers, our students play a role in course selections
            based on their academic goals. The teaching process itself allows students
            to fully participate in learning through ongoing research, self and peer
            evaluation, and a mastering of coursework through our Academic Challenge
            and Student Focus program. The students are thus groomed to become young
            adults who have learned to take responsibility for their own learning,
            giving and accepting constructive feedback, as they move along the learning
            continuum. In this process, students become proficient as self and peer
            evaluators, a process which is vital in learning and professional life.
        </p>

        <p>
            See an illustration of self directed learning by the University of Waterloo.
        </p>
    </div>

    <figure class="col-md-4">
        <img src="<?= BASEPATH ?>/assets/images/academic-programs-1.jpg" alt="Man and lady discussion" class="img-thumbnail">
    </figure>
</div>

<hr />

<h3>Assessment and Evaluation at Bronston</h3>

<p>
    The Assessment and Evaluation process at <abbr title="Bronston Canadian Academy">B.C.A.</abbr>
    ensures that our students are provided with ongoing performance evaluation
    through Formative Assessment.
</p>

<p>
    Following the principles of Formative Assessment, teachers do not progress
    to subsequent lessons unless students have demonstrated a clear and
    consistent command of each subject area. In some cases, it may be
    necessitated for specific students to be provided with extra lessons and
    or custom time tables in order to successfully complete lessons.
</p>

<p>
    This mode of Assessment and Evaluation allows for the learning development
    of each student. Assessment is therefore of the nature that results in
    teacher and student designed interventions towards the acquisition of
    knowledge by both the student and the teacher.
</p>
