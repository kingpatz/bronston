<h2>Post-Graduate Studies</h2>

<section class="col-md-8 left-sec">
    <p>
        International students who wish to apply to College or University for
        Post-Graduate studies may contact us for assistance through the admissions
        process. The process involves applying to Bronston for assistance and paying
        the application fee of $250 to Bronston to initiate the process.
    </p>

    <p>
        An admissions officer will make contact with each applicant through email
        to discuss available options  when the application fee has been received.
        Once the options are clear, the applicant will be required to pay the
        application  fee required for each Post- Grad institution selected for
        admission.
    </p>

    <p>
        There are two ways to gain admission through the Bronston process to a
        Post-Grad program at relevant institutions.
    </p>

    <h3>1. Self  Sponsored</h3>

    <p>
        Provide your own sponsorship towards tuition fees and all other costs,
        depending on the cost of tuition and other costs for each institution
        selected.
    </p>

    <h3>2.  Bronston Sponsored</h3>

    <p>
        Apply for sponsorship through Bronston, through a competitive process as
        noted on the application form. The Bronston sponsored program covers
        Post-Grad programs with no more than one year duration.
    </p>

    <hr />

    <h3>General Costs:</h3>

    <p>
        Bronston Services Application Fee: $250 (Non-refundable and will not be waived).<br />
        Bronston Services Fee: $2500 (This fee is waived for those who win the Bronston Scholarship).<br />
        Post Grad Institution's Application Fee: To be determined at the time of application to the specific institution).
    </p>
</section>

<section class="col-md-4">
    <figure>
        <img src="<?= BASEPATH ?>/assets/images/post-graduate.jpg" alt="Man smiling" class="img-thumbnail">
    </figure>
    <br />
    <p style="text-align: center">
        <a href="https://bronstoncanadianacademy.wufoo.eu/forms/z9g32pg0ur49oi/" target="_blank">Apply for Post-Grad Services</a>
    </p>
</section>

​
