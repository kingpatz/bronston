<h2>Director of Admissions & Accommodation Services</h2>

<section class="col-md-8 left-sec">
    <p class="lh3">
        As the Director of Admissions and Accommodation Services, I welcome
        students to Bronston and ensure they have settled  and live successfully
        within our student residence and home stay accommodations.
    </p>

    <p class="lh3">
        If you are considering the idea of attending school in Canada, I trust
        you will make Bronston your school of choice. I welcome you to view our
        website for guide to our programs and services. If you have further
        questions regarding admissions and accommodation at Bronston, you may
        contact me directly at <a href="mailto:info@bronston.ca">info@bronston.ca</a>.
    </p>
</section>

<figure class="col-md-4">
    <img
        src="<?= BASEPATH ?>/assets/images/director-of-admissions.jpeg"
        class="img-thumbnail" alt="Director of Admissions and Accommodation Services"
    />
    <figcaption>Carole Schaubel</figcaption>
</figure>
