<h2>International Students</h2>

<section class="col-md-7 left-sec">
    <p>
        Bronston is increasonly becoming a good choice for international students
        who prefer to study outside the main city centre in a serene and tranquil
        environment towards advanced studies in Math, Business and or Science with
        strong focus on computer software and coding principles introduced at our
        early years.
    </p>

    <p>
        International students are always welcomed to our campus to participate in
        our academic programs.
    ​</p>

    </p>
        <a href="<?= BASEPATH ?>/admission-process" target="_blank">See our international students application process.</a>
    </p>
</section>

<figure class="col-md-5">
        <img src="<?= BASEPATH ?>/assets/images/three-students-discussing.jpg" alt="Three students discussing" class="img-thumbnail" />
</figure>
