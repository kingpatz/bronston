<h2>About Bronston</h2>

<p>
    Bronston Canadian Academy is a K-12 school located  in Ancaster, Ontario.
    Our <a href="<?= BASEPATH ?>/academic-challenge-program" target="_blank">Academic Challenge Program</a>
    focuses on exposing our students as early as possible to 21st century
    business and industry needs, with focus on advancement in computer
    technology and software development.
</p>

<p>
    Our Mission is to ensure the delivery of quality student centred education
    with a strong focus on teaching learning methodology that will enhance
    computer language development needed for coding and APP development from
    grades 1 to 12.
</p>

<p>
    Recognizing the financial disparities between international students with
    the desire to learn, Bronston Canadian Academy funds special scholarship
    programs to provide financial aid to a select number of students each
    academic year. These scholarships are awarded on a competitive basis.
    See our <a href="<?= BASEPATH ?>/scholarships" target="_blank">Admissions Through Scholarships</a>
    program.
</p>

<p>
    Bronston was founded on the one main principle of respect for humanity,
    <quote>Love Thy Neighbour as Thy Self</quote>. This basic principle resonates
    in all our educational activities.
</p>

<p>
    Our mission is to contribute to the development of creative minds in an
    atmosphere that provokes self  confidence, boldness to achieve and dare
    to believe.
</p>
