<h2>Student Exchange Program</h2>

<section class="row">
    <div class="col-md-8">
        <p>
            Unique to Bronston is our student exchange program designed to add a rich
            cultural mix and experience on our campus.
        </p>

        <p>
            Each year, Bronston welcomes students from diverse backgrounds and cultures
            toparticipate in our programs on a non-credit earning basis.
        </p>

        <p>
            The exchange programs are usually for a duration of one semester or 3
            months in which a student participates fully in our classroom and
            extracurricular activities on a tuition free basis. Students however cover
            the cost of accommodation, meals and transportation for the duration of
            their stay in Canada.
        </p>

        <p>
            To qualify, students must provide a letter of support and reccomendation
            from the principal of the school which they currently attend in their home
            country addressed to the Director of Academics at Bronston Canadian Academy.
            These documents must be signed and submitted through our online
            <a href="https://bronstoncanadianacademy.wufoo.com/forms/znykm330boz7rw/">Student Exchange Application Form</a>.
        </p>

        <p>
            You may contact the Chair of Collaborative Programs directly through email
            at <a href="mailto:admin@bronston.ca">admin@bronston.ca</a> for more
            information regarding the student exchange program.
        </p>
    </div>

    <figure class="col-md-4">
        <img src="<?= BASEPATH ?>/assets/images/student-exchange-program.jpg" alt="Students chatting" class="img-thumbnail">
    </figure>

</section>

<hr />

<h3>Program Cost</h3>

<p>
    The FULL cost of of the student exchange program is based on the cost ofAccommodation, Meals, School Lunch, Transportation and Activities for the entire semester: $7,500 for the duration of the student's stay.<br /><br />
    Application: $150<br />
    Registration Fee: $550
    <br /><br />
    <em>
        *Note that there is no tuition fee with the student exchange program.
    </em>
</p>

<a href="https://bronstoncanadianacademy.wufoo.com/forms/znykm330boz7rw/" target="_blank">APPLY HERE</a>
