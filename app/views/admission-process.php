<h2>Admission Process</h2>

<section class="col-md-6" style="padding-left: 0">
    <p>
        Review our website closely for our programs and services before you apply.
        There are three ways to apply to Bronston Canadian Academy: The General
        Admissions  Stream, the Admissions Through Scholarship Stream and the
        Work-Study/Post-Grad Services Stream.
    </p>

    <hr />

    <h3>General Admissions Stream</h3>

    <p>
        The general admissions stream refers to applicants who wish to apply
        directly for enrolment at Bronston Canadian Academy through self
        sponsorship/fee paying process. For this group of applicants, Bronston
        provides an automatic fee reduction bursary of up to $4,000. The bursary
        is granted without a competitive process and does not depend on
        financial need and or academic background.
    </p>

    <hr />

    <h3>Admissions Through Scholarship Stream</h3>

    <p>
        Students with financial needs may apply for Full or Partial Scholarships
        through the admissions for scholarship program. Our comprehensive
        scholarship programs are highly competitive and awarded to a select
        number of students with demonstrated financial need, academic commitment
        and good behaviour. <a href="<?= BASEPATH ?>/scholarships" target="_blank">Click</a>
        here to review the admissions through scholarship process.
    </p>

    <hr />

    <h3>The Work-Study/Post Grad Stream</h3>

    <p>
        Students who wish to attend Post-Secondary or Post-Grad may apply
        through our Work-Study program if they wish to pursue studies in Nursing.
        <a href="<?= BASEPATH ?>/work-study-in-nursing" target="_blank">Click here</a> to review.
    </p>

    <p>
        Those who do not wish to pursue Nursing but wish to pursue other
        Post-Grad studies through Bronston services may do so through our
        Post-Grad services program. <a href="<?= BASEPATH ?>/post-grad-admission-through-bronston" target="_blank">Click here</a>
        to review.
    </p>

    <hr />

    <h3>Non Refundable Application Fee (All applicants applying to attend Bronston Canadian Academy)</h3>

    <p>
        The application fee of $150 must be paid in all cases in order for your
        application to be processed. This fee cannot be waived through any fee
        waiver process including bursaries and or scholarships.
    </p>

    <hr />

    <h3>Non Refundable Application Fee (Applicants applying to the Work-Study or Post Grad Streams)</h3>

    <p>
        The application fee of $250 must be paid in all cases in order for your
        application to be processed. This fee cannot be waived through any fee
        waiver process including bursaries and or scholarships.
    </p>

    <hr />

    <h3>Application Requirements</h3>

    <p>
        When taking a course at Bronston Canadian Academy, you will need a copy
        of your official transcripts containing the notarized seal of the
        issuing institution. The  transcript is required to ensure you have met
        the prerequisite course requirements for your grade level.
    </p>

    <hr />

    <h3>English Language Requirement</h3>

    <p>
        All courses at Bronston  are taught in English. Proficiency in English
        is a requirement for attendance. If you have been attending a school in
        which the primary language of instruction has been English, you are
        exempted from demonstrating your proficiency in English. International
        students, whose primary language is not English and who have not been
        attending an English language school, would be required to provide a
        demonstrated English language proficiency through
        <abbr title="Test of English as a Foreign Language">TOEFL</abbr> or
        <abbr title="International English Language Testing System">IELTS</abrr>.
    </p>

    <hr />

    <h3>Our Services</h3>

    <p>
        We have implemented systems to ensure a smooth process for our
        international students and applicants:
    </p>

    <p>
        <ul>
            <li>Admissions application support.</li>
            <li>Collaborative-Education partners world-wide to provide local supports.</li>
            <li>Direction and support through study visa application.</li>
            <li>Airport pick-up.</li>
            <li>Accommodation support.</li>
            <li>Bronston Study Plans.</li>
            <li>Programs designed to meet language needs.</li>
            <li>Student integration support through a buddy system.</li>
            <li>International student groups.</li>
            <li>Organized student activities.</li>
        </ul>
    </p>
</section>

<section class="col-md-6" style="padding-right: 0">
    <figure>
        <img
            src="<?= BASEPATH ?>/assets/images/students-using-laptop.jpg"
            class="img-thumbnail" alt="Two students using laptop"
        />
    </figure>
    <br />
    <a href="https://bronstoncanadianacademy.wufoo.eu/forms/w1o6pq4q00b9f1n/" target="_blank">APPLY FOR GENERAL ADMISSION</a><br />
    <a href="https://bronstoncanadianacademy.wufoo.eu/forms/sg8ntt514s00fn/" target="_blank">APPLY THROUGH SCHOLARSHIP</a><br />
    <a href="<?= BASEPATH ?>/work-study-in-nursing" target="_blank">APPLY FOR WORK-STUDY</a><br />
    <a href="<?= BASEPATH ?>/post-grad-admission-through-bronston" target="_blank">APPLY FOR POST-GRADUATE</a><br />
    <a href="https://bronstoncanadianacademy.wufoo.eu/forms/zkxxtzr1a2urm0/" target="_blank">APPLY TO ONLINE SCHOOL</a><br />

    <hr />

    <h3>Study Permit Application Process</h3>

    <p>
        For our assistance with completing the online study permit application,
        click on the data collection link below to provide us with all the
        documentation and information required. Ensure as much accuracy as
        possible in the information and documentation you provide.
    </p>

    <p>
        <a href="https://bronstoncanadianacademy.wufoo.com/forms/z12995zl0ctw9c1/" target="_blank">
            Study Permit Data Collection Form
        </a><br />

        <a href="https://bronstoncanadianacademy.wufoo.com/forms/z1imqln5112t99u/" target="_blank">
            Study Permit Renewal Data Collection Form
        </a>
    </p>
    <p>
        To complete the study permit by yourself, select one of the links below
        depending on your prefered mode of submission.
    </p>

    <p>
        <a href="<?= BASEPATH ?>/study-permit-online-application-guide" target="_blank">
            Study Permit Online Application Guide
        </a>
        <br />
        <a href="https://www.canada.ca/en/immigration-refugees-citizenship/services/application/application-forms-guides/guide-5269-applying-study-permit-outside-canada.html" target="_blank">
            Study Permit Manual Application Process
        </a>
    </p>

    <hr />

    <h3>Fee Payment Process</h3>

    <p>
        All fees, including application fees must be paid through Wire Transfer directly to Bronston Canadian Academy. To make a wire transfer, please go to your local bank and present the Bronston Bank Account information. Your local bank should assist you with the process of wire transfer when you present the Bronston bank account information as noted  below:
    </p>

    <address>
        Beneficiary: Bronston Canadian Academy<br />
        The Bank of Nova Scotia<br />
        630 Upper James Street<br />
        Hamilton Ontario, L9C 2Z1<br />
        Transit # 60442, Institutional # 002<br />
        Acct. # 60442 00779 17<br />
        Swift Code: NOSCCATT<br />
    </address>
</section>
