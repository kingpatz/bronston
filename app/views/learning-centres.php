<h2>Our Learning Centres</h2>

<section class="col-md-8 left-sec">
    <p>
        Bronston Canadian Academy Learning Centres are located in Ghana, Nigeria and Uganda. These centres partner with Bronston Canadian Academy to offer the Canadian pre-university foundation program to students in blended learning environments.
    </p>

    <address>
        Nigeria (Canadian Bridge Academy): Magodo Road, Lagos<br />
        2A, Adekunle Banjo Street, Magodo, Lagos<br />
        Tel. +234 818 816 1307
    </address>

    <address>
        Uganda: (Axial College) Kampala<br />
        Plot 1195 Kibuye-Entebbe Road<br />
        Tel+256 755 767 965
    </address>

    <address>
        Ghana:<br />
        Bronston On-line Learning Services<br />
        Spintex Accra
    </address>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/lady-sitting-library.jpg" alt="Lady sitting on library" class="img-thumbnail" />
</figure>
