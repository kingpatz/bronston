<h2>Road Map to Scholarship.</h2>

<section class="col-md-8 left-sec">
    <h3>Stage 1</h3>

    <p>
        Once your application for admissions with scholarship has been successfully submitted and your application fee is verified, your application will be reviewed by an external board for  selection to attend the second stage of the application process if  successful. Successful applicants will be contacted via email to complete the next process.
    </p>

    <h3>Stage 2</h3>

    <p>
        The second stage of the scholarship application involves the writing of personal essays and submitting some documents. Prepare ahead.
    </p>

    <ul>
        <li>
            Scan two letters of reference from two teachers.
        </li>
        <li>
            Scan one letter of reference from a community leader who has known you for at least 3-4 years.
        </li>
        <li>
            Scan your transcript or school results/records.
        </li>
        <li>
            Scan your type 2-3 page written personal essay which includes the following:
            <br /><br />
            <ol>
                <li>
                    About who you are
                </li>
                <li>
                What you hope to achieve after school
                </li>
                <li>
                How the scholarship will benefit you
                </li>
                <li>
                About your parents and why they are unable to pay your fees
                </li>
                <li>
                How you wish to benefit your community after school
                </li>
            </ol>
        </li>
    </ul>


    <h3>Stage 3</h3>

    <p>
        A final group will be selected after the review of the essays to attend  face to face interviews on Skype as the final stage of the application process.
    </p>

    <p>
        Successful applicants will be selected from those who were invited to the interview. Note that Bronston Club Members receive special consideration at this stage of the selection process.
    </p>

    <h3>Stage 4</h3>

    <p>
        All applicants receive  letters with the results of the scholarship once the process is complete and the successful applicants  for full  scholarships have received their formal letters of award. Those who do not receive the award of full scholarships, are mostly awarded with partial scholarships.
    </p>
    ​
    <p>
        For those students who are awarded partial scholarships but are unable to meet the related financial responsibilities to study abroad at Bronston, the option to complete the Bronston program through On-line schooling and our credit transfer system, will be provided as an avenue to learn at Bronston at a fraction of the cost, noting that the Ontario Secondary School Diploma can be obtained through On-line means and fully accepted for University admissions in several countries.
    </p>

    <hr />

    <h3>Notes for Success:</h3>

    <p>
        The selection board reviews all applications following the criteria closely. To improve your chances for a full scholarship, note that only the best students are selected for full scholarships. Bronston Club Membership is also a positive factor.
    </p>

    <p>
        Those who exhibit signs of independence through the application process, well planned, organized and well written personal essays, good use of the English language, good reference letters and high performance in the face to face interviews, where there is clear display of command of the English language through written and oral communication, as well as good academic reports, are those that will most likely be selected for full scholarships.
    </p>

    <hr />

    <h3>Avenues to Learn at Bronston</h3>

    <p>
        There are several avenues to gaining admission/ learning at Bronston Canadian Academy. If you are not selected for the full scholarship, and cannot afford the partial scholarship, you will be given the opportunity to enrol through the most affordable option, which is the On-line schooling and credit transfer system. Noting that, the Ontario Secondary School Diploma(OSSD) is accepted worldwide by most institutions towards University admissions.
        <br /><br />
        Sincerely,
        <br /><br />
        Admissions Team
    </p>
​</section>

​<section class="col-md-4 right-sec">
    <figure>
        <img src="<?= BASEPATH ?>/assets/images/lady-library-2.jpg" alt="Lady on library" class="img-thumbnail" />
    </figure>
    <br />
    <p>
        Contact us at <a href="mailto:info@bronston.ca">info@bronston.ca</a> to discuss the avenues to learn at Bronston cost effetely!
    </p>
</section>
