<h2>About Our Alumni</h2>

<section class="col-md-8 left-sec">
    <p class="lh3">
        Students enrol in our programs because they have confidence in our
        ability to guide them towards their future both academically and
        professionally. Our students enrol in several University programs
        after completing our program.
    </p>

    <p class="lh3">
        Join Bronston and prepare for enrolment in Canadian Universities.
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/alumni.jpg" alt="Alumni" class="img-thumbnail">
</figure>
