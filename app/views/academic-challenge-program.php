<h2>Academic Challenge Program</h2>

<div class="row">
    <div class="col-md-8">
        <p class="lh2">
            A major part of the teaching, learning methodology within the Bronston Canadian Program, is the Academic Challenge Program (<abbr title="Academic Challenge Program">AC Program</abbr>). The <abbr title="Academic Challenge Program">AC Program</abbr> is an advanced step ahead program designed to:
        </p>

        <ol>
            <li class="lh2">
                Prepare students from all grades to be challenged with advanced
                academic knowledge in computer software technology beyond their
                basic class expectations. This is achieved through the  a teaching
                learning process, curriculum and projects specially designed to
                provide students with the skills and knowledge required to be future
                prepared in a high tech global economy.
                <a href="https://www.youtube.com/watch?v=JDw1ii7aKwg" target="_blank">See hour of coding</a>.
            </li>
        </ol>
    </div>

    <figure class="col-md-4">
        <img src="<?= BASEPATH ?>/assets/images/academic-challenge-program.jpg" alt="Boy and girl on laptop" class="img-thumbnail">
    </figure>
</div>

<ol start="2">
    <li class="lh2">
        To prepare students towards university admissions, with the skills
        required to manage the coursework at the university level once
        admitted. This program starts from grades 9-12 and provides students
        with advanced knowledge in Math, Business and Science. Our normal
        coursework is supplemented  with advanced knowledge in Math, Science
        and Business taught by appropriate and qualified teaching
        professionals. This format of teaching/learning at Bronston provides
        added Academic Challenge to our students and sets the school apart
        from others.
    </li>

    <li class="lh2">
        To prepare students to <strong><q>Step Ahead to University</q></strong>
        recognizing the inadequacies of merely preparing students to meet
        university admissions, by Grade 12, when Bronston students are
        nearing completion of the
        <abbr title="Academic Challenge Program">AC Program</abbr>,
        as an optional aspect of the Academic Challenge Program, grade 12
        students are enrolled in a university course, which focuses on
        advanced reading, writing, comprehension and presentation skills.
        This program is referred to as the
        <strong><q>Step Ahead to University</q></strong> program.
        This may take place either at universities where Bronston students,
        with the  guidance of  student councillors, enroll in a non-degree
        course at the university. The purpose of this part of the
        <abbr title="Academic Challenge Program">AC Program</abbr>
        is to orient students at the university campus and or classroom
        setting, to university coursework and solidify the advanced learning
        skills acquired throughout the
        <abbr title="Academic Challenge Program">AC Program</abbr>.
        Students who graduate successfully from the university course are
        fully prepared and ready to continue on to any university
        environment as degree students, as long as they have  met admission
        requirements.
    </li>
</ol>
