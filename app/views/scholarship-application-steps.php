<h2>Scholarship Application Steps</h2>

<div class="row">
    <div class="col-md-8">
        <p class="lh2">
            The application and competition for the scholarship based admissions are opened to students effective November 21, 2017 and will be closed effective March 31, 2018 for classes beginning in August 2018. Those who win the scholarship awards will be notified in writing via email no later than April 30, 2018 and applications for study permits will be subsequently submitted to CIC.
        </p>
        <p class="lh2">
            For classes beginning in January 2019, the applications for scholarship based admissions will be opened effective April 1, 2018 and closed on July 31, 2018. Those who win the scholarship awards for this period will be notified in writing via email no later than August 31, 2018.

            The scholarships are awarded twice a year for the Fall (August) and Winter (January) admission periods only.
        </p>
        <p class="lh2">
            <em>
                Note that you must complete only one online application form to select all the scholarships and the program you are applying for within one application period.
            </em>
        </p>
    </div>

    <div class="col-md-4" style="text-align: center">
        <img src="<?= BASEPATH ?>/assets/images/students-greeting.jpg" alt="Students greeting" class="img-thumbnail" />
        <br /><br />
        <a href="https://bronstoncanadianacademy.wufoo.eu/forms/sg8ntt514s00fn/" target="_blank">APPLY THROUGH SCHOLARSHIPS</a>
    </div>
</div>

<hr  />

<h3>Before You Apply:</h3>

<ul>
    <li><a href="<?= BASEPATH ?>/road-map-to-scholarship">Click here</a> to review the road map to scholarship page</li>
    <li>Scan your photo ID</li>
    <li>Scan your wire transfer receipt of the application fee of $150 or $250 Canadian, depending on the program you are applying for, made through wire transfer through any bank directly to Bronston (see fee payment process below).</li>
</ul>

<hr  />

<h3>Fee Payment Process</h3>
<p>
    All fees, including application fees must be paid through Wire Transfer directly to Bronston Canadian Academy. To make a wire transfer, please go to your local bank and present the Bronston Bank Account information. Your local bank should assist you with the process of wire transfer when you present the Bronston bank account information as noted  below:
</p>
<address>
    Beneficiary: Bronston Canadian Academy<br />
    The Bank of Nova Scotia<br />
    630 Upper James Street<br />
    Hamilton Ontario, L9C 2Z1<br />
    Transit # 60442, Institutional # 002<br />
    Acct. # 60442 00779 17<br />
Swift Code: NOSCCATT
</address>

<hr  />

<h3>Notes for Success:</h3>
<p>
    The selection board reviews all applications following the criteria closely. To improve your chances for a full scholarship, note that only the best students are selected for this elite full scholarship.
</p>
<p>
    Those who exhibit signs of independence through the application process, well planned, organized and well written personal essays, good use of the English language, good reference letters and high performance in the face to face interviews, where there is clear display of command of the English language through written and oral communication, as well as good academic reports, are those that will most likely be selected for full scholarships.
</p>

<hr  />

<h3>Avenues to Learn at Bronston</h3>
<p>
    There are several avenues to gaining admission/ learning at Bronston Canadian Academy. If you are not selected for the full scholarship, and cannot afford the partial scholarship, you will be given the opportunity to enrol through the most affordable option, which is the On-line schooling and credit transfer system. Noting that, the Ontario Secondary School Diploma(OSSD) is accepted worldwide by most institutions towards University admissions.
</p>
<p>
    We look forward to welcoming you to Bronston through one of our avenues!
</p>
