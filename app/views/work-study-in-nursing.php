<h2>Bronston Graduate Work- Study Program</h2>

<section class="col-md-8 left-sec">
    <p class="lh1">
        The Bronston graduate Work-Study program is designed specifically for
        current Bronston students or internationally educated nurses whose goal is
        to study or continue their studies in Nursing in a Canadian College.
    </p>

    <p class="lh1">
        In a special arrangement with <a href="http://www.millcreekcarecentre.ca/about/" target="_blank">
        Mill Creek Long Term Care</a> <abbr title="Long Term Care">(LTC)</abbr> and similar
        healthcare organizations,
    </p>

    <p class="lh1">
        1. Students who graduate from Bronston may attend College to study Nursing
        while working in healthcare at Mill Creek or other <abbr title="Long Term Care">LTC</abbr>'s to help fund
        their College education.
    </p>

    <p class="lh1">
        2. Internationally educated Nurses may enrol in post-graduate Nurse diploma
        programs in a Canadian College while working either in Mill Creek <abbr title="Long Term Care">LTC</abbr> or
        other similar organizations as arranged by Bronston
    </p>

    <p class="lh1">
        Non Refundable Application Fee: $250 (The  application fee applies to all
        applicants regardless of financial need).
    </p>

    <p class="lh1">
        Work - Study Placement Fee: $5,000
    </p>

    <p class="lh1">
        Students with financial need may apply for placement scholarship through
        the BNEAS program.
    </p>

    <p class="lh1">
        This program is made possible through a Bronston sponsored process.
    </p>

    <p class="lh1">
        For more information about the application process, contact the Bronston
        graduate Work-Study program at <a href="mailto:info@bronston.ca">info@bronston.ca</a>.
    </p>
</section>

<section class="col-md-4">
    <figure>
        <img src="<?= BASEPATH ?>/assets/images/online-school.png" alt="Lady smiling" class="img-thumbnail">
    </figure>
    <br />
    <blockquote>
        <q>
            I live in Ghana while attending Bronston Online to complete
            Pre-University on a partial scholarship program. Because of
            Bronston, I am applying for the Nursing Program at Georgian College
            to study and work at Mill Creek LTC in Canada.
        </q>
    </blockquote>

    <p style="text-align: center">
        <a href="https://bronstoncanadianacademy.wufoo.eu/forms/z98blq70vp7z24/" target="_blank">
            Apply for Work-Study
        </a>
    </p>
</section>
