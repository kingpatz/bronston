<h2>Admissions</h2>

<section class="col-md-8 left-sec">
    <p>
        We encourage you to review the information at our website closely.
        If you are applying from outside of Canada (international student),
        you may also review our <a href="<?= BASEPATH ?>/international-students" target="_blank">International Students'</a>
        page, our <a href="<?= BASEPATH ?>/scholarships" target="_blank">Scholarship Programs</a>
        and the <a href="<?= BASEPATH ?>/scholarships" target="_blank">Study Permit Application Requirements</a>
        for further insight regarding international students’ admissions.
    </p>
    <p>
        Students who are applying through the general process may complete the
        regular application form through any
        <a href="https://bronstoncanadianacademy.wufoo.eu/forms/w1o6pq4q00b9f1n/" target="_blank"><q>APPLY NOW</q></a>
        link located at this website in order to be considered outside the
        scholarship stream. In such cases, students are automatically granted a fee
        reduction bursary of up to $4,000 per academic year.
    </p>

    <p>
        All applicants including those applying through the  scholarship stream,
        MUST pay an application fee of 150 Canadian dollars in order to have their
        applications processed.
    </p>

    <hr />

    <p>
        All fees must be paid directly to Bronston bank account as follows:
    <p/>

    <address>
        The Bank of Nova Scotia<br />
        630 Upper James Street<br />
        Hamilton Ontario, L9C 2Z1<br />
        Transit # 60442, Institutional # 002<br />
        Acct. # 60442 00779 17<br />
        Swift Code: NOSCCATT
    </address>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/walking-student.jpg" alt="Student walking" class="img-thumbnail">
</figure>
