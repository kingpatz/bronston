<h2>Director of Recruiting</h2>

<section class="col-md-8 left-sec">
    <p class="lh2">
        As the director of student recruiting at Bronston, I travel around the
        world consistently to manage student recruitment activities and conduct
        seminars to discuss all that Bronston has to offer regarding education
        and services.
    </p>

    <p class="lh2">
        I also act as liaison between Bronston and students and their parents
        who are interested in pursuing Bronston as an institution of choice.
    </p>

    <p class="lh2">
        I invite you to join our team of students who are serious about pursuing
        their academic goals.
    </p>

    <p class="lh2">
        Welcome to Bronston!
    </p>
</section>

<figure class="col-md-4">
    <img
        src="<?= BASEPATH ?>/assets/images/director-of-recruiting.jpg"
        class="img-thumbnail" alt="Director of Recruiting"
    />
    <figcaption>Elysabeth Gabinet</figcaption>
</figure>
