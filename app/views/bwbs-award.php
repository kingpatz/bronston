<h2>Bronston World Basic Scholarship (BWBS)</h2>

<section class="col-md-8 left-sec">
    <p>
        Bronston offers a grants program through partial scholarships to reduce students' tuition and accommodation fees. Through the <abbr title="Bronston World Basic Scholarship">BWBS</abbr> award, the cost of tuition and accommodation is reduced by 50% for those students who apply for the Admissions Through Scholarships program. Applicants may ask to be considered also for this program if they are not successful for any of the noted full scholarship award programs. The criteria for approval is based on the same competitive process, however the number of students selected is open. This means that more students may qualify for this scholarship depending on financial need, performance in the scholarship assessment and proven academic performance.
    </p>

    <hr />

    <h3>Eligibility Criteria</h3>

    <ol>
        <li>
            Must apply for admission to Bronston Canadian Academy at any grade from grade 7 to Pre-university.
        </li>
        <li>
            Must demonstrate the ability to read and write the English language proficiently.
        </li>
        <li>
            Must be able to demonstrate the commitment required to be successful academically.
        </li>
        <li>
            Must complete all assessments towards the scholarship award.
        </li>
        <li>
            Must be able to demonstrate financial need.
        </li>
        <li>
            Must be able to demonstrate a history of good behaviour in current and previous schools.
        </li>
    </ol>
</section>

<section class="col-md-4 right-sec">
    <img src="<?= BASEPATH ?>/assets/images/bwbs.jpg" alt="Two lady discussing" class="img-thumbnail" />
    <br /><br />
    <p style="text-align: center">
        <a href="<?= BASEPATH ?>/scholarship-application-steps" target="_blank">APPLICATION TIPS</a>
    </p>
</section>
