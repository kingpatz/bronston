<h2>Early Years Program</h2>

<section class="col-md-6 left-sec">
    <p class="lh1">
        Our early years program starts from K to grade 6 and based on the
        Montessori principles.
    </p>

    <p class="lh1">
        The Montessori approach views each child as an individual with unique
        needs from physical, emotional and a cognitive perspective. It is
        child-centred and as such evokes the most success possible for the
        child.
    </p>

    <p class="lh1">
        Our qualified professionals work with each child with parental input to
        ensure the appropriate success plan is developed and implemented to
        meet the academic, cognitive and psychosocial development of the child.
    </p>

    <p class="lh1">
        As a parent, making the decision to enrol your child in the Bronston
        program is one of the best decisions you would ever make for them.
    </p>
</section>

<section class="col-md-6">
    <iframe src="https://player.vimeo.com/video/81134069" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</section>
