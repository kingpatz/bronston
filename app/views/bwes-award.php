<h2>Bronston World Elite Scholarship (BWES)</h2>

<section class="col-md-8 left-sec">
    <p>
        The <abbr title="Bronston World Elite Scholarship">BWES</abbr> is awarded
        to students to attend any program at Bronston Canadian Academy for a
        minimum of 1 year and a maximum of 4 years, between ages 12 - 20 years.​
    </p>

    <hr />

    <h3>What the BWES covers:</h3>
    <ol>
        <li>
            Full cost of tuition for 1-4 academic years at Bronston depending on
            the the grade at which the scholarship is awarded.
        </li>
        <li>
            Full cost of school residence for the duration of the program.
        </li>
        <li>
            Medical insurance coverage for the duration of the program.
        </li>
        <li>
             Monthly bus pass for the duration of the program.
         </li>
        <li>
            A total stipend of up to $1200 per year for the duration of the program.
        </li>
        <li>
            A full coverage of air fair ticket to Canada and back to the student's
            home country upon completion of the Bronston program.
        </li>
    </ol>

    <hr />

    <h3>Eligibility Criteria</h3>
    <ol>
        <li>
            Must apply for admission to Bronston Canadian Academy pre-university or
            student exchange program.
        </li>
        <li>
            Must demonstrate the ability to read and write the English language
            proficiently.
        </li>
        <li>
            Must be at least 12-20 years of age at the time of application.
        </li>

        <li>
            Must have completed at least grade 11 at the time of application if
            applying to attend pre-university at Bronston.
        </li>
        <li>
            Must be able to demonstrate the commitment required to be successful
            academically.
        </li>
        <li>
            Must complete all assessments towards the scholarship award.
        </li>
        <li>
            Must be able to demonstrate financial need.
        </li>
        <li>
            Must be able to demonstrate a history of good behaviour in current and
            previous schools.
        </li>
    </ol>
</section>

<section class="col-md-4 right-sec">
    <img src="<?= BASEPATH ?>/assets/images/bwes.jpg" alt="Lady on laptop" class="img-thumbnail" />
    <br /><br />
    <blockquote>
        <q>
        I came to Bronston Canadian Academy from Ghana a few years ago. I have
        now received the Bronston world elite scholarship award. I will complete
        Bronston Canadian Academy  this year and move on to attend College.
        I am grateful to Bronston
        </q>
    </blockquote>
    <p style="text-align: center">
        <a href="<?= BASEPATH ?>/scholarship-application-steps" target="_blank">APPLICATION TIPS</a>
    </p>
</section>
