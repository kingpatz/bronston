<h2>Coordinator of Academic Services</h2>

<section class="col-md-8 left-sec">
    <p>
        As the coordinator of our academic and health services at Bronston, I
        ensure that students are assessed for their baseline state of health and
        well-being and academic status upon admission to Bronston.
    </p>

    <p>
        At Bronston, we believe that students' academic success is influenced by
        their state of health and well-being. Upon this premise, I focus on
        ensuring that students' health and psychosocial needs are met on an
        ongoing basis.
    </p>

    <p>
        The unique feature of Bronston's style of education ensures the
        recognition of each student as unique in his or her way of learning and
        as such, teaching strategies must be tailored to each individual student
        in a way that respects each student's needs, strengths/abilities and
        weaknesses.
    </p>

    <p>
        The development of each student's Academic Success Plan, is vital upon
        admission and equally vital upon graduation to guide the
        teaching/learning process at Bronston, as well as provide
        recommendations for the student, as he/she progresses to the
        post-secondary level of education.
    </p>

    <p>
        I invite you to review all our programs and services as indicated
        throughout our website. If you have any questions regarding our programs
        and/or the process of admission to Bronston Canadian Academy, please do
        not hesitate to contact me at <a href="mailto:admin@bronston.ca">admin@bronston.ca</a>.
    </p>

    <p>
        I look forward to welcoming you to Bronston where <q>U Direct Learning</q>.
    </p>
</section>

<section class="col-md-4">
    <figure>
        <img
            src="<?= BASEPATH ?>/assets/images/coordinator-of-academic-services.jpg"
            class="img-thumbnail"
        />

        <figcaption>Dorcas Haizel, BScN, RN</figcaption>
    </figure>

    <blockquote>
    ​    <q>
            Every so often in our lives, there is a surprise call. If we
            dare to respond, we are swept into a distance, we are captured,
            to make a difference! Because of Bronston, I am there!
        </q>
    </blockquote>
</section>
