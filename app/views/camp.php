<h2>Camps</h2>

<section class="col-md-6 left-sec">
    <p>
        Join us in our academic and fun filled summer camps on campus and our neibouring conservation parks. Let's learn and have fun too!
    </p>

    <ul>
        <li>Math Camp</li>
        <li>Science Camp</li>
        <li>ESL Camp</li>
    </ul>
    <hr />
    <h3>​​General Program Details</h3>

    <ul>
        <li>Morning indoor or outdoor excercice </li>
        <li>Classroom activities (teaching/learning) of selected subject area: <li>Math, Science and Language Arts and Introduction to APP Coding</li>
        <li>Afternoon break and lunch</li>
        <li>Lectures and workshops</li>
        <li>Evening fun filled activities</li>
        <li>Outdoor fun at the selected conservation parks</li>
        <li>Water parks and activity trips</li>
        <li>Movie nights</li>
    </ul>

    <hr />

    <h3>Program Fee</h3>
    <p>
        Summer Camp Program Fees inlcuding accommodation, meals, tuition, activities and trips for 2 weeks. The fee does not include cost of air fair to and from Canada: $2,950.
    </p>

    <a href="https://bronstoncanadianacademy.wufoo.eu/forms/z1hud9d1o8eodq/" target="_blank">REGISTER HERE</a>
</section>

<section class="col-md-6">
    <div id="camps-carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#camps-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#camps-carousel" data-slide-to="1"></li>
            <li data-target="#camps-carousel" data-slide-to="2"></li>
            <li data-target="#camps-carousel" data-slide-to="3"></li>
        </ol>

        <div class="carousel-inner">
            <div class="item active">
                <img src="<?= BASEPATH ?>/assets/images/camps/78a59b_17e827efbeb249c99d1eac59b3f1f305~mv2_d_5184_3456_s_4_2.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/camps/78a59b_137593eb233148168bf0e19a3d57bdd3~mv2_d_5184_3456_s_4_2.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/camps/78a59b_bbb636daab754101b6f4e2785c168a8a~mv2_d_5184_3456_s_4_2.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/camps/78a59b_f65755d7b77947e18807c406e2c0784c~mv2_d_5184_3456_s_4_2.jpg" />
            </div>
        </div>
    </div>
    <br /><br />
    <div id="camps2-carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#camps2-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#camps2-carousel" data-slide-to="1"></li>
            <li data-target="#camps2-carousel" data-slide-to="2"></li>
            <li data-target="#camps2-carousel" data-slide-to="3"></li>
            <li data-target="#camps2-carousel" data-slide-to="3"></li>
        </ol>

        <div class="carousel-inner">
            <div class="item active">
                <img src="<?= BASEPATH ?>/assets/images/camps2/70b355fb99bb4d0cf4fe311b3fad0e9e.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/camps2/0359a160a6356a953259f6ba61d4b3bf.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/camps2/95953e590864009190b94697add916e5.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/camps2/be9180289069e56115769106141d3ceb.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/camps2/ebb949d0c205d7bbd766b6d2e1cb630f.jpg" />
            </div>
        </div>
    </div>
</section>
