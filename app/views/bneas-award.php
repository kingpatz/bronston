<h2>Bronston Nurse Education Advancement Scholarship (BNEAS)</h2>

<section class="col-md-8 left-sec">
    <p>
        The <abbr title="Bronston Nurse Education Advancement Scholarship">BNEAS</abbr> is a special full scholarship award is in two groups. Group one is awarded to students  to attend grade 12 or pre-university through Bronston. These students must further their education in Nursing either at the diploma or bachelor's degree level depending on the extent of the scholarship awarded.  The scholarship is awarded for a minimum of 1 year to a maximum of 5 years, depending on the extend of the award. It covers the costs of schooling starting with a 1 year program at Bronston Canadian Academy pre-university healthcare focus program to the completion of a nursing program at the post secondary level.  This is a highly competitive and contested scholarship which is awarded to a select number of applicants.
    </p>

    <p>
        The second group  for this award are those who have already completed a nursing degree or diploma program and need to further their education at the post secondary level. The award is for a one year period to attend a post grad program at a College in Canada. It may be a partial or full scholarship award. The application fee however for this group is $250 Canadian dollars.
    </p>

    <hr />

    <h3>What the BNEAS Award Covers</h3>

    <ol>
        <li>
            Full cost of tuition for the duration of the Bronston program.
        </li>
        <li>
            Full cost of tuition for the duration of the diploma or degree nursing program through any approved post secondary institution if extended to include post secondary program.
        </li>
        <li>
            Full cost of Bronston provided residence and meals for the duration of the award.
        </li>
        <li>
            Medical insurance coverage for the duration of the Bronston program.
        </li>
        <li>
            A total stipend of  up to $1200 for the duration of the Bronston program.
        </li>
        <li>
            A full coverage of air fair ticket to Canada and back to the student's home country.
        </li>
    </ol>

    <hr />

    <h3>Eligibility Criteria</h3>

    <ol>
        <li>
            Must apply for admission to Bronston Canadian Academy grade 12 or pre-university  program with health care as the main focus.
        </li>
        <li>
            Must demonstrate the ability to read and write the English language proficiently.
        </li>
        <li>
            Must be at least 17 years and no more than 24 years of age at the time of application.
        </li>
        <li>
            Must have completed at least grade 11 at the time of application.
        </li>
        <li>
            Must be able to demonstrate the commitment required to be successful academically and in nursing education.
        </li>
        <li>
            Must complete all assessments towards the scholarship award.
        </li>
        <li>
            Must be able to demonstrate financial need.
        </li>
        <li>
            Must be able to demonstrate a history of good behaviour in current and previous schools.
        </li>
    </ol>
</section>

<section class="col-md-4 right-sec">
    <img src="<?= BASEPATH ?>/assets/images/bneas.jpg" alt="Lady on laptop" class="img-thumbnail" />
    <br /><br />
    <blockquote>
        <q>
            As one of the the proud recipients of the 2017 scholarship for the advancement of nursing education (BNEAS), I am very thankful to Bronston for making it possible for me to attend private school  in an environment that provides me with 1-1 teacher student environment. I believe this academic challenge opportunity will make it possible for me to gain admission to any  university in Canada to study Nursing. Thank you Bronston.
        </q>
    </blockquote>
    <p style="text-align: center">
        <a href="<?= BASEPATH ?>/scholarship-application-steps" target="_blank">APPLICATION TIPS</a>
    </p>
</section>
