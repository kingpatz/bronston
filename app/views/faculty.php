<h2>Bronston Faculty and Staff</h2>

<section class="col-md-8 left-sec">
  <p>
      Faculty and staff at Bronston are those who share in the vision and
      mission of Bronston Canadian Academy. Our faculty are well educated and
      grounded in the field of education.
  </p>

  <p>
      The passion to guide and support our students through their academic
      journeys is evident and displayed on our campus through the actions of
      our faculty and staff.
  </p>

  <p>
      <a href="<?= BASEPATH ?>/contact-us" target="_blank">Contact Us</a> to join our esteemed
      team.
  </p>
</section>

<figure class="col-md-4">
  <img src="<?= BASEPATH ?>/assets/images/faculty.jpg" class="img-thumbnail" alt="Faculty">
</figure>
