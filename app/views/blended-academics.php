<h2>Blended Learning Environment</h2>

<section class="col-md-8 left-sec">
    <p class="lh2">
        At Bronston we offer our courses through the use of blended learning
        systems including teacherease, rcampus and IXL teaching learning portals.
    </p>

    <p class="lh2">
        Our students can join us from every part of the world through the use of
        our teaching learning portals.
    </p>

    <h3>Student and Teacher Login:</h3>

    <a href="https://ca.ixl.com/" target="_blank">IXL Portal</a><br />

    <a href="https://www.rcampus.com/" target="_blank">Rcampus</a>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/walking-student.jpg" alt="Student walking" class="img-thumbnail">
</figure>
