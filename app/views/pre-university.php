<h2>Pre-University</h2>

<section class="col-md-8 left-sec">
    <p class="lh2">
        Our pre-university program is one of the best in the country.
        With the implementation of our Academic Challenge Step Ahead program,
        our pre-university students receive advanced knowledge in their areas
        of interest and prepare them to be able to meet the challenges of
        post-secondary work.
    </p>

    <p class="lh2">
        Upon the completion of our pre-university  programs, students are
        assisted towards admission to some of the top and world-class
        universities in Canada.
    </p>

    <p class="lh2">
        To benefit from our pre-university program, visit our
        <a href="<?= BASEPATH ?>/admission-process" target="_blank">Admissions Process</a> page
        before you submit your application.
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/pre-university.jpg" alt="Three people studying" class="img-thumbnail" />
</figure>
