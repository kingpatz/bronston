<h2>A Unique Junior High Program</h2>

<section class="col-md-8 left-sec">
    <p class="lh2">
        Our junior high school is unique. It combines academic programs with
        computer sciences and coding to provide students with the knowledge and
        skills required in high tech future work and academic environments.
        Our small class sizes in a multigrade environment means that our
        students are well grounded and adept to high school work at an early
        stage.
    ​</p>

    <p class="lh2">
        To benefit from our junior high program, visit our
        <a href="<?= BASEPATH ?>/admission-process" target="_blank">Admissions Process</a>
        page before you submit your application.
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/junior-high.jpg" alt="Boy and girl with laptop" class="img-thumbnail" />
</figure>
