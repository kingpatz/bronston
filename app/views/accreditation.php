<h2>Accreditation</h2>

<section class="col-md-8 left-sec">
    <p>
        With programs from K-12, Bronston Canadian Academy is an inspected
        private school authorized by the Ministry of Education of Ontario to offer
        credit courses leading to the Ontario Secondary School Diploma (<abbr title="Ontario Secondary School Diploma">OSSD</abbr>).
        The School’s Board Identification or BSID number is 669584.
    </p>

    <hr />

    <h3>Verification of Accreditation</h3>

    <p>
        To verify the school’s authorization to offer credits towards the <abbr title="Ontario Secondary School Diploma">OSSD</abbr>,
        you may <a href="https://www.hamilton.ca/moving-hamilton/education/study-in-hamilton" target="_blank">click here</a>
        to locate Bronston Canadian Academy on the list of recognized
        private schools in the City of Hamilton.
    </p>

    <p>
        <strong>OR</strong>
    </p>

    <p>
        <a href="https://www.ontario.ca/data/private-school-contact-information?_ga=2.267819598.1958228609.1513616698-951252755.1513616698" target="_blank">Click here</a>
        to the Ministry of Education of Ontario website. Once at the site,
        scroll down the page to download data for the list of private schools
        by clicking on the link titled XLSX.
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/lady-library.jpg" alt="Lady in library" class="img-thumbnail">
</figure>
