<h2>Our History</h2>

<p>
    The school begun in the late 1970's as a tutorial school overseas, which
    focused on preparing students for the London GCE exams as well as other
    international exams.
</p>

<p>
    In the early 2000's, still overseas, the school registered and continued
    to prepare students with pre-university programs. Later, the school
    assumed the name of Bronston Canadian Academy and became recognised in
    Canada under the registration and regulatory laws of the Ministry of
    Education of Ontario.
</p>

<p>
    Based on its roots and years of experience in the field of education,
    Bronston Canadian Academy has developed programs to ensure quality of
    education and the advancement of knowledge, in general, through
    international partnerships and initiatives.
</p>

<p>
    We are proud as a faculty body, to realize our roots and nurture our
    branches!
</p>