<h2>Forum for Partners</h2>
<section class="col-md-7 left-sec">
    <p>
        Our annual professional development seminar is held in July each year.<br />
        Fee: $4,800 per person. This includes accomodation and meals for a week.<br />
        Location: Ancaster Ontario.
    </p>

    <h3>Topics to be discussed include:</h3>

    <ul>
        <li>
            The Canadian education systems
        </li>
        <li>
            Collaborative partnership process
        </li>
        <li>
            Cost of partnership
        </li>
        <li>
            Benefits of Partnership
        </li>
        <li>
            The formative classroom
        </li>
        <li>
            Assessment and Evaluation
        </li>
        <li>
            <abbr title="Ontario Secondary School Diploma">OSSD</abbr> and University pathways
        </li>
        <li>
            Partners networking
        </li>
        <li>
            Planning for 2017 admissions
        </li>
        <li>
            Marketing Initiatives
        </li>
        <li>
            Closing with formal dinning
        </li>
    </ul>

    ​<p>
        <em>
            *Partner school representatives, teachers and agent partners are invited to attend.
        </em><br />
        <em>
            *We extend a special welcome to new organizations seeking partnership to attend this forum.
        </em>
    </p>
</section>
<figure class="col-md-5">
  <img src="<?= BASEPATH ?>/assets/images/faculty.jpg" class="img-thumbnail" alt="Two lady discussing">
</figure>
