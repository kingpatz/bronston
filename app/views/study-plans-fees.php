<h2>Bronston Study Plans</h2>
<section class="col-md-7" style="padding-left: 0">​
    <p>
        Recognizing that  students have unique needs, our <q>Bronston Study Plans</q>
        are plan packages designed to provide academic and accommodation service
        options to our students, depending on their  individual academic and
        accommodation needs and preferences.  All students who apply to Bronston
        through the regular application process receive an automatic additional
        $4,000 fee reduction bursary from the noted fees below. The following
        are the different study plans:
    </p>

    <h3>The Bronston Complete Study Plan</h3>

    <p>Services include:</p>

    <ul>
        <li>Residence accommodation with three meals/per day (shared bedrooms and wash-rooms)</li>
        <li>School lunch program</li>
        <li>In house supervision</li>
        <li>School nurse program</li>
        <li>Course materials</li>
        <li>Rigorous academic program</li>
        <li>Academic Challenge program</li>
        <li>In class tuition and tutorials</li>
    </ul>

    <p>
    <strong>
        The total fee for this program is: $27,950 for the first academic year
        and $25,800 there after for students who <em>study at Bronston for more than
        one academic year</em>.
    </strong>
    </p>

    <h3>The Bronston Executive Study Plan</h3>

    <p>
        This study plan is recommended for those students who prefer to stay in private room accommodation.
    </p>

    <p>Services include:</p>

    <ul>
        <li>Residence accommodation (private room)</li>
        <li>Three meals per day</li>
        <li>School lunch program</li>
        <li>In house supervision</li>
        <li>School nurse program</li>
        <li>Course materials</li>
        <li>Rigorous academic program</li>
        <li>In class tuition and tutorial sessions</li>
        <li>Academic Challenge program</li>
    </ul>

    <p>
    <strong>
        The total fee for this program is: $32,950 for the first academic year
        and $28,800 there after for students who <em>study at Bronston for more than
        one academic year</em>.
    </strong>
    </p>


    <h3>The Bronston Executive Plus Study Plan</h3>

    <p>
        This study plan is recommended for those students who require ESL or private 1-1 tutoring.
    </p>

    <p>Services include:</p>

    <ul>
        <li>Residence accommodation (private room)</li>
        <li>Three meals per day</li>
        <li>School lunch program</li>
        <li>In house supervision</li>
        <li>School nurse program</li>
        <li>Course materials</li>
        <li>Rigorous academic program</li>
        <li>In class tuition and tutorial sessions</li>
        <li>Academic Challenge program</li>
        <li><abbr title="English as a Second Language">ESL</abbr> or Private tutoring</li>
    </ul>

    <p>
    <strong>
        The total fee for this program is: $35,950 for the first academic year
        and $33,800 there after for students who <em>study at Bronston for more than
        one academic year</em>.
    </strong>
    </p>

    <p>
        All students who apply through the normal application process receive an
        automatic fee reduction bursary of $4,000 per academic year.
    </p>

    <a href="<?= BASEPATH ?>/bronzi-program" target="_blank">See the (Bronzi Program)</a>.
</section>

<section class="col-md-5" style="padding-right: 0">
    <div id="study-plans-fees-carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#study-plans-fees-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#study-plans-fees-carousel" data-slide-to="1"></li>
            <li data-target="#study-plans-fees-carousel" data-slide-to="2"></li>
            <li data-target="#study-plans-fees-carousel" data-slide-to="3"></li>
            <li data-target="#study-plans-fees-carousel" data-slide-to="4"></li>
            <li data-target="#study-plans-fees-carousel" data-slide-to="5"></li>
        </ol>

        <div class="carousel-inner">
            <div class="item active">
                <img src="<?= BASEPATH ?>/assets/images/study-plan-fees/4dd3a24f28247b71b2d2bc4fc45851e9.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/study-plan-fees/fe7c63d6306ec3f076aabdef5a19bfaf.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/study-plan-fees/d08d6d9a0b4b7f3852d60ced7eca5206.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/study-plan-fees/64a908ade301fbc3bb67ddc335858a1d.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/study-plan-fees/31768e78c9a154776f6d539beecedd7a.jpg" />
            </div>

            <div class="item">
                <img src="<?= BASEPATH ?>/assets/images/study-plan-fees/0b4641d48ea74af88e59dd39e190d3f2.jpg" />
            </div>
        </div>
    </div>
    <br />
    <p>
        Summer Camp Program Fees inlcuding accomodation, meals, tuition,
        activities and trips for 3 weeks:
    </p>

    <p>
    Math Camp: $3,850<br />
    Science Camp: $3,950<br />
    ESL Camp: $3,850
    </p>

    <h3>Fee Payment Process</h3>

    <p>
        All fees, including application fees must be paid through Wire Transfer
        directly to Bronston Canadian Academy. To make a wire transfer, please
        go to your local bank and present the Bronston Bank Account information.
        Your local bank should assist you with the process of wire transfer
        when you present the Bronston bank account information as noted below:
    </p>

    <p>
        Beneficiary: Bronston Canadian Academy<br />
        The Bank of Nova Scotia<br />
        630 Upper James Street<br />
        Hamilton Ontario, L9C 2Z1<br />
        Transit # 60442, Institutional # 002<br />
        Acct. # 60442 00779 17<br />
        Swift Code: NOSCCATT
    </p>
</section>
