<h2>What Our Alumni Are Doing?</h2>

<section class="row alumni-testimonial">
    <blockquote class="col-md-8 lh3">
        <q>
            Needing a second career, I enrolled in Bronston Canadian Academy
            On-line school program to prepare for my journey towards  a
            career in nursing. I was impressed with the specialized quality
            program designed for students seeking enrolment to Nursing
            schools and the attention and guidance I received to propel me
            towards admission into the Nursing program at Niagara College.
            I am so grateful that because of Bronston, I am close to
            realizing my dream.
        </q>
    </blockquote>

    <figure class="col-md-4">
        <img
            src="<?=BASEPATH ?>/assets/images/alumni-testimonial-1.jpg"
            class="img-thumbnail"
        />
    </figure>
</section>
<hr />
<section class="row alumni-testimonial">
    <blockquote class="col-md-8 lh3">
        <q>
            I have always had the dream to be a Pharmacist so I applied to
            Bronston as an international student and gained admission.
            Bronston provided me with the supports and teaching I needed to
            gain admission to MacEwan University. I am in the first year BSc
            program and preparing to enrol in school of Pharmacy upon
            graduation. Because of Bronston, I am on my way to becoming a
            Pharmacist.
        </q>
    </blockquote>

    <figure class="col-md-4">
        <img
            src="<?=BASEPATH ?>/assets/images/alumni-testimonial-2.jpg"
            class="img-thumbnail"
        />
    </figure>
</section>
<hr />
<section class="row alumni-testimonial">
    <blockquote class="col-md-8 lh3">
        <q>
            I enrolled in the Bronston program to prepare for admission to a
            nursing program. With the rigorous 1-1 teacher student program,
            I was able to focus and increase my grades remarkably to compete
            for admission to a nursing program. Because of Bronston, I have
            now completed my nursing program and proceeding to write the
            College of Nurses of Ontario licensing exam.
        </q>
    </blockquote>

    <figure class="col-md-4">
        <img
            src="<?=BASEPATH ?>/assets/images/alumni-testimonial-3.jpg"
            class="img-thumbnail"
        />
    </figure>
</section>
