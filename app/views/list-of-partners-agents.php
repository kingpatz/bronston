<h2>Educational Institution Partners</h2>

<section class="col-md-6 left-sec">
    <ul class="lh2">
        <li>
            Canadian Bridge Academy (Lagos, Nigeria)
        </li>
        <li>
            Axial College (Uganda)
        </li>
        <li>
            Bronston University College, Ghana
        </li>
    </ul>

    <h2>Agent Partners</h2>

    <ul class="lh2">
        <li>
            WR Consultancy (Uganda)
        </li>
        <li>
            Mr. Ben Obeng (Ghana)
        </li>
        <li>
            Ms. Ela Gabinet (Canada)
        </li>
        <li>
            Rest Assured Services (Nigeria)
        </li>
    </ul>
</section>

<figure class="col-md-6">
  <img src="<?= BASEPATH ?>/assets/images/faculty.jpg" class="img-thumbnail" alt="Two lady discussing">
</figure>
