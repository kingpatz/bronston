<h2>Language Studies</h2>

<section class="col-md-8 left-sec">
    <p class="lh2">
        Our language program is effective in transitioning students to study in
        English. With the combination of classroom work and social interaction
        program, our students receive advanced knowledge and the skills
        required to prepare them to be able to meet the challenges of
        post-secondary work and/or enter the job market.
    </p>

    <p class="lh2">
        If you are interested in pursuing English or French as a second language
        (<abbr title="English as a Second Language">ESL</abbr> or
        <abbr title="French as a Second Language">FSL</abbr>), contact us to start your  journey towards the improvement
        of your language skills.
    </p>

    <p class="lh2">
        To benefit from our languages program, visit our
        <a href="<?= BASEPATH ?>/admission-process" target="_blank">Admissions Process</a> page
        before you submit your application.
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/language-studies.jpg" alt="Two ladies discussing" class="img-thumbnail" />
</figure>
