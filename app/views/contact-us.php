<h2>Contact Us</h2>

<section class="col-md-6 left-sec">
    <h3>Email</h3>

    <p>
        admin@bronston.ca<br />
        info@bronston.ca
    </p>
</section>

<section class="col-md-6 right-sec">
    <h3>Have Questions or Suggestions?</h3>
    <form action="" method="post">
        <input type="text" placeholder="Name" name="name" class="form-control" /><br />
        <input type="email" placeholder="Email" name="email" class="form-control"/><br />
        <input type="text" placeholder="Subject" name="subject" class="form-control"/><br />
        <textarea placeholder="Message" name="message" class="form-control" style="resize: none" rows="8"></textarea><br />
        <button type="submit" class="btn pull-right" style="background: red; color: white">Send</button>
    </form>
</section>
