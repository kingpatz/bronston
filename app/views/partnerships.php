<h2>Collaborative-Edu Partnership</h2>

<section class="col-md-8 left-sec">
    <p>
        Our Collaborative Education (Collaborative-Edu) Partnership is a partnership agreement between educational institutions/recruiting agents and  Bronston Canadian Academy.
    </p>

    <p>
        The Collaborative-Edu program offers partnerships with those who wish to join hands with Bronston Canadian Academy towards the mission to spread quality education worldwide. Whether you are a representative of an educational institution, academic agent, consultant or an entrepreneur, you can benefit from our Collaborative-Edu partnership program. You may contact us at <a href="mailto:admin@bronston.ca">admin@bronston.ca</a> to discuss your partnership interests.
    </p>

    <p>
        For complete details regarding our Collaborative-Edu program, please download the  Bronston Collaborative – Edu brochure for your review.
    </p>

    <p>
        If you are a representative of an institution, you will be contacted by Mr. S. Doran, the Director of the Collaborative Partnerships, who will assist you through the process of becoming a partner.
    </p>
</section>

<section class="col-md-4" style="text-align: center">
    <figure>
      <img src="<?= BASEPATH ?>/assets/images/faculty.jpg" class="img-thumbnail" alt="Two lady discussing">
    </figure>
    <br />
    <a href="https://bronstoncanadianacademy.wufoo.com/forms/z1nl39he1i3v29e/" target="_blank">APPLY FOR PARTNERSHIP</a>
</section>
