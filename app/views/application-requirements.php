<h2>Application Requirements</h2>

<div class="row">
    <div class="col-md-8">
        <p>
            Bronston Canadian Academy enrols students from  elementary to
            Pre-university, as well as provide services to students who wish to enrol
            at other Colleges or Universities through the Work-Study or Post-Graduate
            programs.
        </p>

        <p>
            To be admitted to Bronston Canadian Academic directly, applicants are
            required to provide documented proof of prior education through academic
            reports or transcripts with corresponding picture ID's.
        </p>

        <p>
            Upon admission to Bronston Canadian Academy, all students are required to
            write an entrance exam, the results of which is used to determine the
            appropriate grade level for each applicant.
        </p>

        <p>
            In some cases, applicants may be required to repeat prior grades depending
            on their academic records and or Bronston exam results.
        </p>

        <p>
            Application requirements to other Colleges and Universities depend on each
            institution. Applicants will be advised at the time of application regarding
            specific application requirements of relevant institutions.
        </p>
    </div>
    <div class="col-md-4" style="text-align: center">
        <figure>
            <img src="<?= BASEPATH ?>/assets/images/walking-student.jpg" alt="Student walking" class="img-thumbnail" />
        </figure><br />

        <a href="<?= BASEPATH ?>/avenues-to-enrolment" target="_blank">REVIEW YOUR OPTIONS</a>
    </div>
</div>

<hr  />

<h3>Bronston Application Requirements</h3>

<ul>
    <li>Completed online application form</li>
    <li>Application fee of $150 (Canadian)</li>
    <li>Government issued picture ID such as health card or passport</li>
    <li>Current academic report with marks or transcript</li>
</ul>

<hr  />

<h3>General Application Requirements for <a href="<?= BASEPATH ?>/work-study-in-nursing" target="_blank">Work-Study​</a> program</h3>

<ul>
    <li>Completed online application form</li>
    <li>Application fee of $250 (Canadian)</li>
    <li>Graduates of Bronston Canadian Academy</li>
    <li>Graduates of any Nursing Certificate/Nurse Assistant, Nursing Diploma or Nursing Degree programs</li>
    <li>Specific College/University application requirements.</li>
</ul>

<hr  />

<h3>Application Requirements for <a href="<?= BASEPATH ?>/post-grad-admission-through-bronston" target="_blank">Post-Graduate</a> programs</h3>

<ul>
    <li>Completed online application form</li>
    <li>Application fee of $250 (Canadian)</li>
    <li>Graduates of any Bachelor Degree program</li>
</ul>

<hr  />

<h3>Fee Payment Process</h3>

<p>
    All fees, including application fees must be paid through Wire Transfer
    directly to Bronston Canadian Academy. To make a wire transfer, please go
    to your local bank and present the Bronston Bank Account information. Your
    local bank should assist you with the process of wire transfer when you
    present the Bronston bank account information as noted  below:
</p>

<p>
Beneficiary: Bronston Canadian Academy<br />
The Bank of Nova Scotia<br />
630 Upper James Street<br />
Hamilton Ontario, L9C 2Z1<br />
Transit # 60442, Institutional # 002<br />
Acct. # 60442 00779 17<br />
Swift Code: NOSCCATT
</p>
