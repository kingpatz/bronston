<h2>Home School Program</h2>

<section class="col-md-8 left-sec">
    <p class="lh1">
        Our home school program is part of our Bronston Blended Academics program
        <abbr title="Bronston Blended Academics">(BBA)</abbr>.Through this program,
        home schooled students may enroll as students of <abbr title="Bronston Canadian Academy">B.C.A.</abbr>
        and complete courses from K-12 towards the eventual graduation with the
        Ontario Secondary School Diploma.
    </p>

    <p class="lh1">
        This program offers a direct partnership arrangement between Bronston and
        the parents of home schooled children, providing a structured curriculum,
        resources, and if preferred, a platform for student group participation
        through online means. This structure is designed to meet the academic needs
        of home schooled students both locally and internationally.
    </p>

    <h3>Ontario Home Schooling Policy Guidelines</h3>

    <p class="lh1">
        If you are resident of Ontario considering home education for your child,
        you may review the relevant guidelines to assist you through the legal process.
    </p>
</section>

<figure class="col-md-4">
    <img
        src="<?= BASEPATH ?>/assets/images/home-schooling.jpg"
        class="img-thumbnail" alt="Teacher teaching student"
    />
</figure>
