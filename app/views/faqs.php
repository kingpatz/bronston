<h2>FAQs</h2>

<dl>
    <dt>
        1. Can I travel to Canada to attend Bronston as an international student?
    </dt>
    <dd>
        Bronston is a campus based school which delivers its programs both
        through a live and on-line campus. Bronston also partners with
        international schools to deliver courses on-line.
    </dd>

    <dt>
        2. If my child has a study permit, can I travel to Canada with my child?
    </dt>
    <dd>
        Parents of children studying in Canada may travel to Canada with
        their children during the durantion of the child or children's
        education. It must be noted however that it is not automatic to
        receive a visitor's permit once your child or children have received
        study permits to study in Canada.
    </dd>

    <dt>
        3. As a parent of a child who is studying in Canada, can I work while in Canada?
    </dt>
    <dd>
        Parents whose children are students in Canada may work ONLY AFTER
        <abbr title="Human Resources Development Canada">HRDC</abbr>
        approval. This means that employers of such parents have to
        seek approval for each parent or visitor the employer wants to
        employ. The employer must show evidence that no Canadian is
        available to fill the position available. It is therefore not easy
        to obtain employment while staying in Canada as a visitor.
    </dd>


    <dt>
        4. What is the difference between on-line learning and campus based
        learning? How does it work?
    </dt>
    <dd>
        <p>
        Study through on-line Learning at Bronston Canadian Academy is for
        those students who wish to gain a qualification without having to
        attend traditional classes. This means that you can study in a
        flexible way that fits around your lifestyle no matter your location.
        </p>
        <p>
        On campus learning means that students attend campus based classes
        based on planned time tables.
        </p>
        <p>
        <abbr title="Bronston Canadian Academy">B.C.A.</abbr> relies on
        on-line course materials and format to deliver its on-line learning
        programs. There is an on-line library, on-line course calendar,
        internet and database services. All books are available as e-books
        and can be accessed or purchased on the school’s library website.
        </p>
    </dd>

    <dt>
        5. Is the school accredited?
    </dt>
    <dd>
        Bronston Canadian Academy is an inspected private school and has the
        full authorization by the Ministry of Education of Ontario to offer
        Ontario elementary to secondary school courses towards the Ontario
        Secondary School Diploma.
    </dd>

    <dt>
        6. Will my credentials be accepted after I graduate from B.C.A.?
    </dt>
    <dd>
        Yes, <abbr title="Bronston Canadian Academy">B.C.A.</abbr> courses
        lead to the Ontario Secondary School Diploma
        (<abbr title="Ontario Secondary School Diploma">OSSD</abbr>) which
        is issued by the Ministry of Education of Ontario. The Diploma is
        accepted for university admissions worldwide including Canada.
    </dd>

    <dt>
        7. Do I have to pay all fees at once?
    </dt>
    <dd>
        International students must pay all fees before admission. All fees
        with the exception of application and administration fees, are
        refunded when students' visas are refused.  Tuition fees are
        otherwise non-refundable.
    </dd>

    <dt>
        8. Is there a scholarship program?
    </dt>
    <dd>
        Yes. We do offer some scholarships and bursaries. See our admissions through scholarships.
    </dd>

    <dt>
        9. Is it possible to travel to Canada to attend Bronston?
    </dt>
    <dd>
        Yes, Bronston Canadian Academy operates through a campus in
        Hamilton, Ontario and welcomes international students to study at
        the campus or online. The school  has supports in place to direct
        students through their visa and travel process, including
        accommodation.
    </dd>

    <dt>
        10. What is the duration of the courses?
    </dt>
    <dd>
        The courses are based on the Ministry of Education of Ontario
        policies. Full or 1 credit courses are 110 hours in duration and
        half credit courses are 55 hours in duration.
    </dd>

    <dt>
        11. If I take my program fully on-line, will there be opportunity
        to interact with other students in the program?
    </dt>
    <dd>
        Yes, you could have group members with whom you could engage in
        discussion on-line or in person if arranged by students.
    </dd>

    <dt>
        12. What happens if I am not doing well in my courses?
    </dt>
    <dd>
        Bronston has academic counsellors and teachers trained to provide
        supports and guidance to students as they progress through their
        courses. Through formative assessment practices, students are
        provided with adequate, timely and 1-1 supports as needed to
        successfully complete courses.
    </dd>
    <dt>
        13. Do you have services for Post-Secondary and Post-Grad students?
    </dt>
    <dd>
        Yes we do. See:
        <a href="<?= BASEPATH ?>/post-grad-admission-through-bronston" target="_blank">
            Post-Graduate Studies
        </a>
    </dd>
</dl>
