<h2>Bronzi Program</h2>
<div class="row">
    <div class="col-md-8">
        <p>
            A unique feature of the teaching learning methodology within the
            Bronston Canadian program, is the specialized Bronzi program. This
            program offered mainly from grades 11-12, is specially designed for
            those students who need to study in very small focused limited class
            groups of <strong>one to five students per class</strong>.
        </p>

        <p>
            The main objective of the Bronzi program is to prepare students for
            enrollment into elite university programs that demand extraordinarily
            high grades for admission such as Engineering, Health/Medical Sciences
            and elite Business programs etc.
        </p>

        <p>
            While all Bronston Canadian Academy programs prepare students towards
            high academic achievements, the Bronzi program is an intense and closely
            mentored program to meet the needs of students who require close
            mentorship and intense academic focus in very small groups.
        </p>
    </div>

    <figure class="col-md-4">
        <img src="<?= BASEPATH ?>/assets/images/academic-challenge-program.jpg" alt="Boy and girl on laptop" class="img-thumbnail">
    </figure>
</div>

<p>
    Students who have not been successful at other schools have had proven
    success in the Bronzi program and have transformed from prior failed
    grades to achieving in the 90% grade point averages upon completion of
    the Bronzi program.
</p>

<p>
    Although most students fair well in the regular Bronston programs, for
    those who require a specialized intense academic program, parents will
    be advised upon enrollment of their children if the Bronzi program is
    the right choice for them.
</p>

<section>
    <h3>The Bronzi Program Fees:</h3>

    <p>
        <strong>
        Academic and accommodation fees: $65,000/ Academic year<br />
        One time registration: fee $2,000<br />
        One time assessment fee: $2,000
        </strong>
    </p>
</section>
