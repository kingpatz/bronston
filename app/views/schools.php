<h2>Schools</h2>

<p class="col-md-8 left-sec lh3">
    Our programs are divided into early years, junior high school,
    high school, pre-university and languages schools.
</p>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/schools.jpeg" alt="Two kids" class="img-thumbnail">
</figure>
