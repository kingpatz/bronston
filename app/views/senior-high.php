<h2>High School Program</h2>

<section class="col-md-8 left-sec">
    <p class="lh2">
        The high school program focuses on preparing students for post secondary
        admissions in reputable institutions. Bronston courses are offered on a
        credit basis and results in the Ontario Secondary School Diploma. Our
        1-1 teacher student teaching learning process provides the opportunity
        for each student's academic growth.
    </p>

    <p class="lh2">
        To benefit from our high school program, visit our
        <a href="<?= BASEPATH ?>/admission-process" target="_blank">Admissions Process</a> page
        before you submit your application.
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/senior-high.jpg" alt="Two ladies with book" class="img-thumbnail" />
</figure>
