<h2>On-line Campus</h2>

<section class="col-md-8 left-sec">
    <p class="lh2">
        Besides our campus based learning, the Bronston programs are fully
        available On-line through our On-line Learning Management System (Rcampus).
    </p>

    <p class="lh2">
        Students may enrol On-line from any country or place in the world and
        complete the courses and exams towards the award of the Ontario Secondary
        School Diploma.
    </p>

    <p class="lh2">
        Studying On-line means that international students need not worry about
        study visa issues as well as the high cost of travelling to study at our
        campus in Canada.
    </p>

    <hr />

    <h3>Mode of Study</h3>

    <p class="lh2">
        Courses may be taken based on amount of credits each student requires to
        obtain the Ontario Secondary School Diploma <abbr title="Ontario Secondary School Diploma">(OSSD)</abbr>,
        or simply to qualify for University admissions. Students who have completed
        secondary school and do not need the <abbr title="Ontario Secondary School Diploma">OSSD</abbr>
        may take as few as one to two credit courses if that is all they require for
        University admission.
    </p>

    <p class="lh2">
        Students are always advised to consult the Universities to which they wish
        to apply to inquire about the admission requirements before enrolling in
        any Bronston program.
    </p>
</section>

<section class="col-md-4">
    <figure>
        <img src="<?= BASEPATH ?>/assets/images/online-school.png" alt="Lady smiling" class="img-thumbnail">
    </figure>
    <br />
    <blockquote>
        <q>I live in Ghana while attending Bronston On-line school to complete the
            OSSD.  Because of Bronston, I am applying to attend Georgian College to
            study Nursing in Canada on the Work-Study program
        </q>
    </blockquote>
    <p style="text-align: center">
        <a href="https://bronstoncanadianacademy.wufoo.eu/forms/zkxxtzr1a2urm0/" target="_blank">
            APPLY TO BRONSTON
        </a>
    </p>
</section>
