<h2>Bronston World Elite Plus Scholarship (BWEPS)</h2>

<section class="col-md-8 left-sec">
    <p>
        The <abbr title="Bronston World Elite Plus Scholarship">BWEPS</abbr> is
        awarded for a minimum of 2 years and maximum of 5 years, with 1 year
        completed at Bronston and the  rest extending beyond Bronston to the
        completion of  1 - 2 year diploma programs and 4 year degree programs,
        depending on the extent of the award.
    </p>

    <hr />

    <h3>What the BWEPS Award Covers</h3>

    <ol>
        <li>
            Full cost of tuition for the duration of the award to attend Bronston.
        </li>
        <li>
            Full cost of tuition for the duration of the award to attend any
            approved post secondary institution if the award is an extended award.
        </li>
        <li>
            Full cost of Bronston provided residence and meals for the duration of
            the award (Bronston to post secondary).
        </li>
        <li>
            Medical insurance coverage for the duration of the Bronston program.
        </li>
        <li>
            Monthly bus pass for  the duration of the Bronston program.
        </li>
        <li>
            A total stipend of  up to $1200 for the duration of the Bronston program.
        </li>
        <li>
            A full coverage of air fair ticket to Canada.
        </li>
    </ol>

    <hr />

    <h3>Eligibility Criteria</h3>

    <ol>
        <li>
            Must apply for admission to Bronston Canadian Academy pre-university
            program at the time of application for the scholarship.
        </li>
        <li>
            Must demonstrate a clear ability to read and write the English language
            proficiently.
        </li>
        <li>
            Must have completed grade 11 or 12 at the time of application and be
            eligible for admission to the Bronston pre-university program.
        </li>
        <li>
            Must be between 16-24 years of age at the time of application.
        </li>
        <li>
            Must be able to display the commitment required to succeed in an
            academic  program.
        </li>
        <li>
            Must complete all assessments towards the scholarship award.
        </li>
        <li>
            Must be able to demonstrate financial need.
        </li>
        <li>
            Must be able to demonstrate a history of good behaviour in current and
            previous schools including the general community.
        </li>
    </ol>
</section>

<section class="col-md-4 right-sec">
    <img src="<?= BASEPATH ?>/assets/images/bweps.jpg" alt="Lady reading" class="img-thumbnail" />
    <br /><br />
    <blockquote>
        <q>
            I am very happy to be awarded with the BWEPS scholarship award to attend Bronston Canadian Academy from grade 10. I am from Uganda and this award has given me the chance to attend school in Canada. I plan to do my best to do well academically. I want to say a very big thank you to Bronston.
        </q>
    </blockquote>
    <p style="text-align: center">
        <a href="<?= BASEPATH ?>/scholarship-application-steps" target="_blank">APPLICATION TIPS</a>
    </p>
</section>
