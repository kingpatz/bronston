<h2>The Bronston Scholars Club Membership</h2>

<section class="row">
    <div class="col-md-8">
        <p class="lh3">
            Bronston Scholars Club is a Bronston education and services membership which provides a unique avenue to members to benefit from all Bronston education and services. There are several benefits to joining Bronston's unique education membership designed to promote quality education at an early age to students globally. Regardless of the system of education in which a student decides to learn, the Bronston membership can be incorporated to increase each student's learning skills and choices in education.
        </p>
        <a href="https://bronstoncanadianacademy.wufoo.eu/forms/m1xzlrvo06fig4j/" target="_blank">Register for Membership</a>
    </div>

    <figure class="col-md-4">
        <img src="<?= BASEPATH ?>/assets/images/female-asian-student.jpg" alt="Asian student" class="img-thumbnail">
    </figure>
</section>
<hr />

<h3>Bronston Scholars Club Membership Benefits</h3>

<ul>
    <li>
        Membership in the Bronston Scholars Club provides students with the
        access code to all of Bronston's online educational curriculum,
        lectures/lessons, videos, print and library resources etc.
    </li>
    <li>
        Members may use Bronston lessons in Math, English, Science, Social
        Sciences and the Arts etc, to prepare for any related course and or
        exams or to transfer later to the Canadian education system as
        needed.
    </li>
    <li>
        Members may apply to attend Bronston any time and be considered at
        20% to 30% discount from the regular tuition and accommodation fees,
        depending on years of membership.
    </li>
    <li>
        Members receive 20%-30% discount on all other services provided to
        those seeking Work-Study and Post Graduate  programs.
    </li>
    <li>
        As a general rule, all Bronston services are provided to Bronston
        Club Menbers at a 20%-30% discount.
    </li>
    <li>
        Members may seek admission to  Bronston Through Scholarships and pay
        20%-30% reduction towards the scholarship application fee. Note
        that, club members are given special consideration as part of the
        scholarship selection criteria.
    </li>
    <li>
        At a 20%-30% discounted cost, members who wish to obtain the
        Ontario Secondary School Diploma will be able to write the Ontario
        Secondary School Literacy Test after the transfer of their credits
        to Bronston towards the diploma, through Prior Learning Assessment
        and Recognition
        (<abbr title="Prior Learning Assessment and Recognition">PLAR</abbr>).
        The test may be written in the member's home country or the most
        conducive location arranged by Bronston.
    </li>
</ul>

<hr />

<h3>Criteria for Membership</h3>

<ul>
    <li>
        Students may join from kindergarten to University.
    </li>
    <li>
        Adults who intend to apply for Work-Study or Post-Graduate programs
        may join to benefit from reduction of service fees.
    </li>
    <li>
        Parents of member students benefit from Bronston services and need
        not join.
    </li>
</ul>

<hr />

<h3>Membership Fee</h3>

<ul>
    <li>
        Four years duration: $75
    </li>
    <li>
        13 year duration from K-12: $225
    </li>
</ul>

<hr />

<h3>Note</h3>

<ul>
    <li>
        All Bronston students are Bronston Scholars Club members
        automatically and may opt to maintain their membership status after
        completing Bronston Canadian Academy.
    </li>
    <li>
        However, not all Bronston Scholar's Club members  are Bronston
        students. You don't need to be a Bronston student to become a
        Bronston Scholar!
    </li>
</ul>
