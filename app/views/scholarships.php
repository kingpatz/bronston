<h2>Admissions Through Scholarships</h2>

<section class="col-md-8 left-sec">
    <p>
        The Bronston  Scholarship Awards provide full and partial scholarships on a
        financial needs basis to students across the world. These are competitive
        scholarship awards based on specific criteria. The scholarships were awarded
        to a select number of successful applicants from five different countries to
        attend Bronston Canadian Academy in the 2017/2018 academic year.
    </p>

    <p>
        The application and competition for the scholarship based admissions are
        opened to students effective November 21, 2017 and will be closed effective
        March 31, 2018 for classes beginning in August 2018. Those who win the
        scholarship awards will be notified in writing via email no later than
        April 30, 2018.
    </p>

    <p>
        For classes beginning in January 2019, the applications for scholarship
        based admissions will be opened effective April 1, 2018 and closed on
        July 31, 2018. Those who win the scholarship awards for this period will be
        notified in writing via email no later than August 31, 2018.
    </p>

    <p>
        The scholarships are awarded twice a year for the Fall (August) and Winter
        (January) admission periods only. Students who win any form of scholarship
        award at Bronston may be expected to provide written and or audio visual
        testimonials through the school's website, social media and TV and ADs when
        required.
    <p>

    <p>
        Non refundable application fee for grades 7-12 applicants: $150<br />
        Non refundable application fee for post graduate nurse applicants (BNEAS): $250
    </p>

    <p>
        Click on the following links below to review the different scholarships.
    </p>

    <ol>
        <li>
            <a href="<?= BASEPATH ?>/bwes-award" target="_blank">
                Bronston World Elite Scholarship (BWES)
            </a>
        </li>
        <li>
            <a href="<?= BASEPATH ?>/bweps-award" target="_blank">
                Bronston World Elite Plus Scholarship (BWEPS)
            </a>
        </li>
        <li>
            <a href="<?= BASEPATH ?>/bwjs-award" target="_blank">
                Bronston World Junior Scholarship (BWJS)
            </a>
        </li>
        <li>
            <a href="<?= BASEPATH ?>/bneas-award" target="_blank">
                Bronston Nurse Education Advancement Scholarship (BNEAS)
            </a>
        </li>
        <li>
            <a href="<?= BASEPATH ?>/bwbs-award" target="_blank">
                Bronston Partial Scholarship (BWBS)
            </a>
        </li>
    </ol>

    <hr />

    <h3>Fee Payment Process</h3>

    <p>
        All fees, including application fees must be paid through Wire Transfer
        directly to Bronston Canadian Academy. To make a wire transfer, please go
        to your local bank and present the Bronston Bank Account information. Your
        local bank should assist you with the process of wire transfer when you
        present the Bronston bank account information as noted  below:
    </p>

    <address>
        Beneficiary: Bronston Canadian Academy<br />
        The Bank of Nova Scotia<br />
        630 Upper James Street<br />
        Hamilton Ontario, L9C 2Z1<br />
        Transit # 60442, Institutional # 002<br />
        Acct. # 60442 00779 17<br />
        Swift Code: NOSCCATT
    </address>
</section>

<section class="col-md-4" style="text-align: center">
    <img src="<?= BASEPATH ?>/assets/images/boy-girl-laptop.jpg" alt="Boy and girl on laptop" class="img-thumbnail" />
    <br /><br />
    <a href="<?= BASEPATH ?>/admission-process" target="_blank">APPLY THROUGH ADMISSION</a>
</section>
