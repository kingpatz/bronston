<h2>Accomodation</h2>

<section class="col-md-8 left-sec">
    <p>
        Generally, our  student  accommodation involves the provision of furnished
        rooms with bed, dresser, table and internet services.
    </p>

    <h3>Student Residence:</h3>

    <ol>
        <li>
            A general student residence which houses 10-15 students under the
            supervision of a Bronston residence staff.
        </li>
        <li>
            Home Stay accommodation which houses up to 3 students in a semi family
            based supervised home setting.
        </li>
    </ol>

    <p>
        There are nutritious meals provided daily to students including a school
        lunch on school days.
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/room-study.jpg" alt="Student studying in his room" class="img-thumbnail">
</figure>
