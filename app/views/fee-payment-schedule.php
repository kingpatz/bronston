<h2>How to Pay Bronston Fees</h2>

<section class="col-md-8 left-sec">
    <p>
        All fees, including application fees must be paid through Wire Transfer
        directly to Bronston Canadian Academy. To make a wire transfer, please go
        to your local bank and present the Bronston Bank Account information. Your
        local bank should assist you with the process of wire transfer when you
        present the Bronston bank account information as noted  below:
    </p>

    <address>
    Beneficiary: Bronston Canadian Academy<br />
    The Bank of Nova Scotia<br />
    630 Upper James Street<br />
    Hamilton Ontario, L9C 2Z1<br />
    Transit # 60442, Institutional # 002<br />
    Acct. # 60442 00779 17<br />
    Swift Code: NOSCCATT
    </address>

    <hr />

    <h3>Fee Schedule</h3>

    <p>
        The fee structure at <abbr title="Bronston Canadian Academy">B.C.A.</abbr>
        has been designed to provide clarity for students and parents regarding the
        cost of studying at Bronston and living in Canada.
    </p>

    <p>
        <em>
        * Study Plan Packages are available to provide students with complete costs
        of studying at Bronston depending on the plan they select. Students may opt
        for either one of the following Study Plans (Complete, Executive or Bronzi).
        </em>
    </p>

    <p>
        <em>
        * For the fees for summer camps, scroll down to the bottom of this page.
        </em>
    </p>

    <p>
        <em>
        * Students who select a Study Plan do not pay extra fees beyond the cost of
        the plan they select.
        </em>
    </p>

    <hr />

    <h3>List of Compulsory Services & Fees:</h3>

    <p>
        Bronston Application Fee: The Application fee is charged to students on a
        one time basis as long as the student’s file remains current at the school.
        Students who cancel their enrollment status and wish to return to the
        school are subject to the noted application fee. All application fees MUST
        be paid in full before any student application is processed.
    </p>

    <p>
        <b>Bronston Program Application Fee:</b> $150<br />
        <b>Work-Study Services Application Fee:</b> $250
            <a href="<?= BASEPATH ?>/work-study-in-nursing" target="_blank">(See other Work-Study services fees)</a><br />
        <b>Post-Graduate Services Application Fee:</b> $250
            <a href="<?= BASEPATH ?>/post-grad-admission-through-bronston" target="_blank">(See other Post-Graduate services fees)</a>
    </p>

    <hr />

    <h3>Registration Fee:</h3>

    <p>
        A one time pre-registration fee of $2,000 MUST be paid to <abbr title="Bronston Canadian Academy">B.C.A.</abbr>
        in full, before the school issues acceptance letters to international
        students who wish to study through <abbr title="Bronston Canadian Academy">B.C.A.</abbr> campus.
    </p>

    <hr />

    <h3>Tuition and Academic Services Fee:</h3>

    <p>
        The tuition fee cover the cost of tuition. Different tuition fees apply for
        different programs.
    </p>

    <p>
        Kindergarten-Grade 6: $14,000/year<br />
        Grades 7-8: $15,800/year<br />
        Grade  9-12: $15,800/year<br />
        Single Course: $2,000/Course<br />
        ESL & FSL: $1,000/Level. ($8,000 per year)
    </p>

    <hr />

    <h3>Residence Accommodation Fee:</h3>

    <p>
        Student residence is provided at a fee of $800/month for shared room or
        $1100 per month for private room. Residence is a a good choice for students
        who prefer to stay in a student based community. There are three full
        meals/day provided in residence.
    </p>

    <hr />

    <h3>Home Stay Accommodation Fee:</h3>

    <p>
        Home Stay is a type of residence based on family living and provided to
        students at a fee of $750/month. Students who prefer to stay with a
        Canadian family while studying in Canada, enjoy a family home environment.
    </p>

    <p>
        Home Stay Liaison Fee: $100/Month<br />
        Nursing Services: $2,000 per year
    </p>

    <hr />

    <h3>List of Optional Programs & Fees</h3>

    <h4>ESL Program Fee:</h4>

    <p>
        A fee of $3,000 per semester is charged to students towards ESL classes.
    </p>

    <h4>1:1 Tutorial Fee:</h4>

    <p>
        A fee of $3,000/semester is charged to students who prefer or require extra
        1-1 tutoring.
    </p>

    <h4>
        Summer Camp Program Fees inlcuding accommodation, meals, tuition, activities
        and trips for 2 weeks. The fee does not include cost of air fair to and from
        Canada:
    </h4>

    <h5>Camp Type</h5>

    <ul>
        <li>Math/Science</li>
        <li>Language Arts Camp with Fun and Activities</li>
    </ul>

    <p>
        Fee: $2,950 (for 2 weeks camp duration)
    </p>

    <hr />

    <h3>Fee Policy</h3>

    <p>
        With the exception of the school's security deposit fee, no other fees at
        Bronston Canadian Academy is refundable after the student has received the
        study permit. The security deposit fee is refunded following the assessment
        of damages incurred by the student. Should the damages incurred be more
        than the security deposit fee, the student will receive a bill to remit the
        additional fee within 30 business days.
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/female-asian-student.jpg" alt="Asian student" class="img-thumbnail" />
    <br /><br />
    <img src="<?= BASEPATH ?>/assets/images/academic-programs-1.jpg" alt="Two people using laptop" class="img-thumbnail" />
    <br /><br />
    <img src="<?= BASEPATH ?>/assets/images/language-studies.jpg" alt="Two students discussing" class="img-thumbnail" />
    <br /><br />
    <img src="<?= BASEPATH ?>/assets/images/happy-student-using-laptop.jpg" alt="Happy student using laptop" class="img-thumbnail" />
</figure>
