<h2>Avenues to Enrolment</h2>

<div class="row">
<div class="col-md-8">
    <p>
    There are a few Avenues to gain admission to study at Bronston Canadian
    Academy or to use Bronston's services towards College or University
    admission.
    </p>

    <hr />

    <h3>The First Avenue:</h3>

    <p>
        Is to apply as a fee paying student. In this case, you pay your own
        fees and provide your own sponsorship towards a study permit. This is
        the most straight forward process to gain admission. All you need to do
        is to complete the <a href="https://bronstoncanadianacademy.wufoo.eu/forms/w1o6pq4q00b9f1n/" target="_blank">
        General Application</a> form and submit along with your
        school transcript or results and the relevant non refundable application
        fee of $150.
    </p>
</div>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/students.jpg" alt="Lady in library" class="img-thumbnail">
    <br />
    <a href="<?= BASEPATH ?>/admission-process" target="_blank">Apply to Bronston</a>
</figure>
</div>
<hr />

<h3>The Second Avenue:</h3>

<p>
    Is through applying and competing for full scholarship. This avenue is
    not guaranteed. Meaning that, you may not be selected among the pool of
    those who win the full scholarship.  It is competition based and not
    all applicants will win. To enrol through this avenue, complete the
    <a href="https://bronstoncanadianacademy.wufoo.eu/forms/sg8ntt514s00fn/" target="_blank">
    Admissions Through Scholarships</a> form and submit along with the non
    refundable application fee of $150.
</p>

<hr />

<h3>The Third Avenue:</h3>

<p>
    Is through the award of partial scholarships for those students
    applying to enrol as Bronston students directly. The potential or
    chances for the award  of partial scholarships is much higher.
    This means that, most students who apply through scholarship to attend
    Bronston directly, have a high chance of receiving the partial
    scholarship award. To be considered for the partial scholarship,
    complete the same <a href="https://bronstoncanadianacademy.wufoo.eu/forms/sg8ntt514s00fn/" target="_blank">
    Admissions Through Scholarship</a> form with the non
    refundable application fee of $150.
</p>
​
<p>
    Note that, those who apply to attend Colleges and Universities through
    the Work-Study or the Post- Grad program do not win partial
    scholarships automatically as in the case of those applying to attend
    Bronston directly. This means that, the award of partial scholarships
    as an alternative to full scholarships in the case of the
    Work-Study/Post-Grad applicants, <strong>is not a guarantee</strong>.
    Complete either the
    <a href="https://bronstoncanadianacademy.wufoo.eu/forms/z98blq70vp7z24/" target="_blank">
    Work-Study</a> or <a href="https://bronstoncanadianacademy.wufoo.eu/forms/z9g32pg0ur49oi/" target="_blank">
    Post-Grad</a> application form if you fall under this category and
    submit with the non application fee of $250. (Post-Grad applicants are
    those who have completed any University Degree program)
</p>

<hr />

<h3>The Forth Avenue:</h3>

<p>
    Is through enrolment  to study at Bronston through the On-line school
    at an affordable fee. This option is only open to students applying to
    attend Bronston directly as Bronston students. Most students who cannot
    afford to pay partial fees to attend Bronston and do not receive the
    full Scholarship award, may attend Bronston On-line school at a very
    low and affordable cost to obtain the Ontario Secondary School Diploma.
    The Ontario Secondary School Diploma is acceptable worldwide at most
    Universities and Colleges as  admission requirement. To apply directly
    to attend Bronston On-line school, complete the
    <a href="https://bronstoncanadianacademy.wufoo.eu/forms/zkxxtzr1a2urm0/" target="_blank">
    On-line Study Application</a> form and submit along with the application
    fee of $150.
</p>

<hr />

<h3>The Fifth Avenue:</h3>

<p>
    Is to enrol through one of our partner institutions located in Uganda,
    Ghana and Nigeria. <a href="<?= BASEPATH ?>/learning-centres" target="_blank">
    Click here</a> to find a Bronston partner.
</p>

<hr />

<h3>The Sixth Avenue:</h3>

<p>
    Is through the Bronston Club Membership to obtain specific discounts
    for Bronston education and services while using Bronston teaching
    learning systems and resources.
    <a href="<?= BASEPATH ?>/bronston-scholars-club" target="_blank">Click here</a> to
    review the club membership
    process.
</p>

<hr />

<h3>Important Notes:</h3>

<p>
    The best age range for study permit success to Canada:

    <ul>
        <li>
            For those applying to attend Bronston directly in Canada is 20
            years,
        </li>

        <li>
            24 years, if studying with Bronston On-line and planning to
            come to Canada to attend College or University,
        </li>

        <li>
            28 years, if coming to Canada directly to College,
        </li>

        <li>
            And 30-32 years if coming to University for Post-Graduate
            studies.
        </li>
    </ul>

    <strong>
        The age range provided does not affect enrolment at Bronston.
        However, for those intending to travel to study in Canada, these
        age ranges are important to note for study permit purposes.
    </strong>
</p>

<hr />

<h3>Fee Payment Process</h3>

<p>
    All fees, including application fees must be paid through Wire Transfer
    directly to Bronston Canadian Academy. To make a wire transfer, please
    go to your local bank and present the Bronston Bank Account information.
    Your local bank should assist you with the process of wire transfer
    when you present the Bronston bank account information as noted below:
</p>

<address>
    Beneficiary: Bronston Canadian Academy<br />
    The Bank of Nova Scotia<br />
    630 Upper James Street<br />
    Hamilton Ontario, L9C 2Z1<br />
    Transit # 60442, Institutional # 002<br />
    Acct. # 60442 00779 17<br />
    Swift Code: NOSCCATT
</address>
