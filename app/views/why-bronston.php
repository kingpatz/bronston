<h2>Why Bronston?</h2>

<h3>Internationally Recognized Education</h3>
<p>
    The Bronston Academic Challenge program is based on the Ontario
    Elementary to Secondary School curriculum with an advanced focus in
    Math, Business and Science.  The Canadian education system is ranked
    among the world’s best education systems. This is where Bronston awards
    the Ontario Secondary School Diploma (<abbr title="Ontario Secondary School Diploma">OSSD</abbr>).
</p>

<hr />

<h3>Academic  Achievement Step Ahead Program</h3>
<p>
    Setting Bronston apart is the Academic Challenge Step Ahead program
    which prepares students with the advanced knowledge and skills required
    for success at the university level.
</p>

<hr />

<h3>1-1 Student Focus in Small Class Sizes</h3>
<p>
    Learning through the Bronston Academic Challenge program focuses on each
    student as an individual who is able to think reflectively, critically
    and above all, to be a life-long learner. Each student has specific
    skills that he/she can harness and build upon. Our curriculum is
    delivered in a conducive and cognitively stimulating environment that
    will inspire your child to participate.
</p>

<hr />

<h3>Qualified Educators</h3>
<p>
    Bronston teachers are well qualified with Ontario teaching certification
    and/or extensive teaching experience to implement our Teaching/Learning
    Methodology.
</p>

<hr />

<h3>Assessment and Evaluation</h3>
<p>
    As qualified and experienced professionals, <abbr title="Bronston Canadian Academy">B.C.A.</abbr> teachers, promote
    and implement a variety of teaching and learning strategies, along with
    different types of assessment and evaluation methods to ensure all
    students experience exceptional and measurable learning outcomes. With
    a strong focus on formative assessment to drive the teaching process,
    our students’ learning needs are addressed to promote their academic
    success.
</p>

<hr />

<h3>Learning Management Systems</h3>
<p>
    The Bronston course management system permits teachers and learners to
    share current teaching/learning resources in live and on-line classrooms
    while promoting peer-peer and teacher-student interactions.
</p>

<hr />

<h3>Parent Engagement</h3>
<p>
    By design, the Bronston program encourages parental involvement in the
    education and learning process through the RCampus portal.
</p>

<hr />

<h3>University Admission Prospects</h3>
<p>
    Students who complete the Bronston program successfully are awarded the
    <abbr title="Ontario Secondary School Diploma">OSSD</abbr> and gain
    admissions to universities worldwide including: Canada, the <abbr title="United States of America">USA</abbr>,
    <abbr title="United Kingdom">UK</abbr> and Australia.
</p>

<hr />

<h3>Entrance Scholarships</h3>
<p>
    ALL students who complete the Bronston program successfully, with at
    least 80% average, automatically qualify for entrance scholarship in any
    Canadian university upon admission. This scholarship is awarded by the
    relevant universities upon the student’s admission, with no need to
    complete a scholarship application.
</p>

<hr />

<h3>University Admission Supports</h3>
<p>
    Students who complete the Bronston program, are supported through the
    University/College admission process.
</p>
