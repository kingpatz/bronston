<h2>OSSD</h2>

<section>
    <h3>Requirements To Earn Ontario Secondary School Diploma (OSSD)</h3>

    <p>
        In order to earn an Ontario Secondary School Diploma
        (<abbr title="Ontario Secondary School Diploma">OSSD</abbr>), a student
        must earn a minimum of 30 credits, including 18 compulsory credits and
        12 optional credits. Students must also complete 40 hours of community
        involvement activities and must pass the Ontario Secondary School
        Literacy Test, or successfully complete the Ontario Secondary School
        Literacy Course
        (<abbr title="Ontario Secondary School Literacy Course">OSSLC</abbr>).
    </p>
</section>

<section>
    <h3>Compulsory Credits* (Total of 18)</h3>
    <h4>Subjects</h4>
    <ul>
        <li>
            4 English (1 credit per grade)***
        </li>

        <li>
            1 French-as-a-second language
        </li>

        <li>
            3 Mathematics (at least 1 in grade 11 or 12)
        </li>

        <li>
            2 science
        </li>

        <li>
            1 Canadian history
        </li>

        <li>
            1 Canadian geography
        </li>

        <li>
            1 The arts
        </li>

        <li>
            1 Health and physical education
        </li>

        <li>
            0.5 Civics
        </li>

        <li>
            0.5 Career studies
        </li>

        <li>
            1 Additional English or a third language or social sciences and the
            humanities or Canadian and world studies or guidance and career
            education or cooperative education
        </li>

        <li>
            1 Additional credit in health and physical education or the arts or
            business studies or cooperative education
        </li>

        <li>
            1 Additional credit in science (grade 11 or 12) or technological
            education (grades 9 – 12) or cooperative education
        </li>

        <li>
            A credit is granted when a course of at least 110 hours is completed
            with a pass
        </li>

        <li>
            All courses listed have a 1.0 credit value unless otherwise
            indicated (e.g. 0.5 credit)
        </li>

        <li>
            A maximum of 3 ESL credits may be counted towards the 4 required
            English credits
        </li>
    </ul>
</section>

<section>
    <h3>Optional Credits (Total of 12)</h3>

    <p>
        In addition to the 18 compulsory credits, students must earn 12 optional
        credits. Students may earn these credits by successfully completing
        courses that they have selected from the courses listed by grade in the
        Academic Program.
    </p>
</section>

<section>
    <h3>40 hours of Community Involvement Activities:</h3>

    <p>
        As part of the diploma requirements, students must complete a minimum
        of 40 hours of community involvement activities.
    </p>
    <p>
        These may take place in a variety of settings (business, not-for-profit
        organizations, public sector institutions <i>(including hospitals)</i>,
        and informal settings).
    </p>

    <p>
        Students may not fulfill the requirement through activities that are
        counted towards an academic credit, through paid work or by assuming
        duties normally performed by a paid employee. The requirement is to be
        completed outside students’ normal instructional hours.
    </p>

    <p>
        A variety of activities and opportunities to fulfill these hours will
        be made available to the students by the school throughout the year.
        Students are to maintain and provide a record of their community
        involvement. Such records must be confirmed by the organization or
        persons supervising the activities and submitted to the Academic
        Coordinator. These records will be submitted to the Principal for
        approval upon the completion of the 40 hours.
    </p>
</section>

<section>
    <h3>Ontario Secondary School Literacy Test (OSSLT)</h3>

    <p>
        Each student is required to write an Ontario Secondary School Literacy
        Test during his/her grade 10 year, or the year (grade 10-12) in which
        they enroll for the first time in an Ontario secondary school.
        The student must achieve an acceptable level in both reading and
        writing in order to receive his/her diploma. If students do not achieve
        an acceptable level of achievement in the literacy test they will be
        required to rewrite the test until they receive a satisfactory result.
        The student’s result will be recorded on his/her student transcript.
        Under special circumstances students may be granted <em>a deferral or a
        rewrite</em>.
    </p>

    <p>OR</p>
</section>

<section>
    <h3>Ontario Secondary School Literacy Course Grade 12</h3>

    <p>
        This credit course is designed to support at-risk students in improving
        their language skills and provide them with an alternative way of
        demonstrating these skills. Students who have had at least two
        opportunities to write the OSSLT and who have failed it at least once
        will be eligible to take the course. The successful completion of this
        course will satisfy the literacy requirement for graduation. Students
        who obtain a graduation diploma by passing the literacy course will be
        required to demonstrate a standard of reading and writing skills
        comparable to those measured by the
        <abbr title="Ontario Secondary School Literacy Test">OSSLT</abbr>.
        The
        <abbr title="Ontario Secondary School Literacy Test">OSSLT</abbr>
        is the foundation for the design of this course.
    </p>
</section>

<section>
    <h3>Substitutions for Compulsory Credits</h3>

    <p>
        The Principal of the school may substitute up to three compulsory
        courses with courses from the remainder of those that meet the
        compulsory credit requirement. Substitutions will be made to promote
        and enhance student learning or to meet special needs and interests.
    </p>
</section>

<section>
    <h3>Prerequisites</h3>

    <p>
        If a course has a prerequisite requirement, this prerequisite must be
        attained prior to the enrollment in the course. Satisfactory achievement
        in prerequisite courses is crucial to the full understanding and
        successful completion of subsequent courses. However, following
        discussion with the student’s parents and teachers, the Principal may
        allow a student to proceed in a particular course without first
        obtaining the normal pre-requisite.
    </p>

    <p>
        This decision will be made only if it is in the student’s best interest,
        and the Principal, the teachers and the parents are in agreement. Since
        the pre-requisite has not been met, there may be substantial challenges
        for the student, and although support from the school is available, it
        is the responsibility of the student to ensure that the course is
        successfully completed.
    </p>

    <p>
        Appropriate records for waiving of pre-requisites, showing all of the
        appropriate permissions, will be maintained in the student's
        <abbr title="Ontario Student Record">OSR</abbr>.
    </p>
</section>