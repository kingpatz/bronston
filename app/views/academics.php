<h2>Academic Programs</h2>
<section class="col-md-7 left-sec">
    <p>
        Our Academic programs are carefully and professionally designed to meet
        the needs of our students. Bronston Canadian Academy programs prepare
        students, through every step in the academic journey, to assume the
        challenges of university studies and social life.
    </p>

    <p>
        As a private institution registered and inspected by the Ministry of
        Education of Ontario, Bronston  follows the Ministry of Education
        Curriculum and policies closely to ensure that students receive the
        quality of education they each deserve.
    </p>

    <p>
        Our teachers are those who have the passion, experience and command of
        the subjects they teach.
    </p>

    <h3>University Preparation</h3>

    <p>
        Our career advancement team focuses on the several pathways available
        to students after they graduate from Bronston Canadian Academy.
        Students are directed to complete course credits based on their career
        goals and directed to university programs most suitable to them.
    </p>

    <h3>Mode of Study</h3>

    <p>
        Combining our electronic and live class teaching systems ensures that
        our students are abreast with current technology in education and have
        broad knowledge and understanding of the world around them.
    </p>
</section>

<figure class="col-md-5">
    <img src="<?= BASEPATH ?>/assets/images/academic-programs-1.jpg" alt="Man and lady discussion" class="img-thumbnail">
    <br /><br />
    <img src="<?= BASEPATH ?>/assets/images/academic-programs-2.jpg" alt="Lady teaching girl" class="img-thumbnail col-md-6">
    <img src="<?= BASEPATH ?>/assets/images/academic-programs-3.jpg" alt="Kids lab experiment" class="img-thumbnail col-md-6">
</figure>
