<h2>Important Study Permit Considerations</h2>

<section class="col-md-8 left-sec">
    <p class="lh1">
        The main point to note  is that, to be successful with the  study permit,
        you must show a very strong bank account for at least 6 months before you
        submit the application. Although the immigration looks for at least $10,000
        in your bank account for least 6 months, we advice parents to ensure that
        they have at least close to $30,000 in their bank account for better support
        of their children in Canada.
    </p>

    <p class="lh1">
        The funds should be in the account for at least 6 months and the bank
        account MUST show ongoing activities. Parents or sponsors need to ensure
        that their bank accounts reflect at least 6 months history of account
        activity.
    </p>

    <p class="lh1">
        To prevent their victimization through fraudulent activities, parents and
        or sponsors MUST pay all fees directly to Bronston bank account.
    </p>

    <p class="lh1">
        <b>Bank Information:</b><br />
        <address style="display: inline">
            The Bank of Nova Scotia<br />
            630 Upper James Street<br />
            Hamilton Ontario, L9C 2Z1<br />
            Transit # 60442, Institutional # 002<br />
            Acct. # 60442  00779 17<br />
            Swift Code: NOSCCATT
        </address>
    ​</p>

    <p class="lh1">
        <b>Email:</b> <a href="mailto:info@bronstonca">info@bronston.ca</a>
    </p>

    <p class="lh1">
        For further information regarding study and temporary work permits, please
        select the following CIC sites:
        <br />
        <a href="http://www.cic.gc.ca/english/study/study.asp" target="_blank">
            http://www.cic.gc.ca/english/study/study.asp
        </a>
        <br />
        cic temporary work permit
    </p>
</section>

<figure class="col-md-4">
    <img src="<?= BASEPATH ?>/assets/images/post-graduate.jpg" alt="Man smiling" class="img-thumbnail" />
    <br /><br />
    <img src="<?= BASEPATH ?>/assets/images/student-exam.jpg" alt="Student taking exam" class="img-thumbnail" />
</figure>
