<h2>FSL</h2>

<section class="col-md-8 left-sec">
    <p class="lh1">
        Our <abbr title="French as a Second Language">FSL</abbr> program is
        designed to prepare students with the language skills required at the
        university level to  pursue a program in French Language or simply to
        prepare students  with the skills required for social communication and/or
        entry into the job market.
    </p>

    <p class="lh1">
        Students are placed at the appropriate level after careful evaluation of
        their French language abilities, both written and verbal.
    </p>

    <p class="lh1">
        Each level of competence requires 5 weeks of intensive classroom and social
        preparation to complete successfully. There are a total of 15 Levels which
        a student must complete to receive our full competency certification.
    </p>

    <p class="lh1">
        Note that students may start at any level as long as they are determined
        appropriate for the given level based on the Bronston language testing
        system.
    </p>

    <p class="lh1">
        Students completing any language program at Bronston are placed in Home
        Stay residence with host families who are profecient in the given language.
    </p>
</section>

<section class="col-md-4" style="text-align: center">
    <figure>
        <img src="<?= BASEPATH ?>/assets/images/female-asian-student.jpg" alt="Asian student" class="img-thumbnail">
    </figure>
    <br />
    <a href="https://bronstoncanadianacademy.wufoo.com/forms/w1o6pq4q00b9f1n/" target="_blank">
        APPLY HERE
    </a>
</section>
