<h2>Bronston World Junior Scholarship (BWJS)</h2>

<section class="col-md-8 left-sec">
    <p>
        The <abbr title="Bronston World Junior Scholarship (BWJS)">BWJS</abbr> is awarded to grades 7 and 8 students towards short to full year programs at Bronston, up to and including college or university completion. This scholarship may be applied to our short summer camps and exchange programs.
    </p>

    <hr  />

    <h3>What the BWJS Award Covers</h3>

    <ol>
        <li>
            Full cost of tuition for the duration of the award.
        </li>
        <li>
            Full cost of tuition for the duration of the award if extended to include the duration of post secondary program.
        </li>
        <li>
            Full cost of Bronston provided residence and meals for the duration of the award.
        </li>
        <li>
            Medical insurance coverage for the duration of the Bronston program.
        </li>
        <li>
            A total stipend of  up to $1200 for the duration of the Bronston   program.
        </li>
        <li>
            A full coverage of air fair ticket to Canada and back to the   student's home country.
        </li>
    </ol>

    <hr />

    <h3>Eligibility Criteria</h3>

    <ol>
        <li>

            Must apply for admission to Bronston Canadian Academy junior high school or exchange  program.
        </li>
        <li>

            Must demonstrate the ability to read and write the English language proficiently.
        </li>
        <li>

            Must be at least 10 years and no more than 13 years of age at the time of application.
        </li>
        <li>

            Must have completed grade 6 or 7 at the time of application.
        </li>
        <li>

            Must be able to demonstrate the commitment required to be successful academically.
        </li>
        <li>

            Must complete all assessments towards the scholarship award.
        </li>
        <li>

            Must be able to demonstrate financial need.
        </li>
        <li>

            Must be able to demonstrate a history of good behaviour in current and previous schools.
        </li>
    </ol>
</section>

<section class="col-md-4 right-sec">
    <img src="<?= BASEPATH ?>/assets/images/bwjs.jpg" alt="Teacher and student" class="img-thumbnail" />
    <br /><br />
    <p style="text-align: center">
        <a href="<?= BASEPATH ?>/scholarship-application-steps" target="_blank">APPLICATION TIPS</a>
    </p>
</section>
