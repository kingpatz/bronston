<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Init {
    private $controller;

    function __construct() {
        $url = explode('?', $_SERVER['REQUEST_URI']);
        $url = @strtolower($url[0]);
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        $controller = (empty($url[1])) ? DEFAULT_CONTROLLER : $url[1];
        $file = ROOT . DS . 'app' . DS . 'controllers' . DS . $controller
            . '.php';

        if(!file_exists($file)) {
            $controller = DEFAULT_ERR_CONTROLLER;
            $file = ROOT . DS . 'app' . DS . 'controllers' . DS . $controller
                . '.php';
        }

        require $file;
        $controller = ucfirst(str_replace('-', '_', $controller));
        $this->controller = new $controller();

        $action =
            (!empty($url[2]) && method_exists($this->controller, $url[2])) ?
            $url[2] : DEFAULT_ACTION;

        $this->controller->$action();
    }

    function __destruct() {
        unset($this->controller);
    }
}
