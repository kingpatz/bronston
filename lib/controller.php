<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

abstract class Controller {

	protected $view;
	protected $load;

	function __construct() {
		$this->load = new Loader();
	}

	function __destruct() {
		unset($this->load);
	}
}