<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

// database constants
define('DB_HOST', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', '');

// defaults
define('DEFAULT_CONTROLLER', 'home');
define('DEFAULT_ERR_CONTROLLER', 'err');
define('DEFAULT_ACTION', 'index');

// timezone
date_default_timezone_set('Asia/Manila');
