<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class View {

	function __construct($file, $data) {
		require_once ROOT . DS . 'app' . DS . 'views' . DS . 'includes' . DS . 'header.php';
		require_once $file;
		require_once ROOT . DS . 'app' . DS . 'views' . DS . 'includes' . DS . 'footer.php';
	}
}