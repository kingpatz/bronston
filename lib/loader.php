<?php if(!defined('BASEPATH')) die('Direct script access not allowed.');

class Loader {

	function view($view, $data) {
		$view = strtolower($view);
		$file = ROOT . DS . 'app' . DS . 'views' . DS . $view . '.php';
		if(file_exists($file)) {
			return new View($file, $data);
		} return false;
	}
}