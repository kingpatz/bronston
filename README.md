## Dependencies ##

### Linux / Ubuntu ###
* Apache
* PHP

### Windows ###
* XAMPP / WAMP

## Server Set Up ##

### Linux / Ubuntu ###
* Copy or move this directory to **/var/www/html**
* Enter the following commands in the terminal:
```
sudo service apache2 restart
```

### Windows ###
* Copy or move this directory to **C:\xampp\htdocs** *(XAMPP)* / **C:\wamp\www** *(WAMP)*
* Restart XAMPP / WAMP
